package com.psybergate.grad2021.oo_part_1.ce4a1;

public class Utils {

    public static void main(String[] args) {
        Account accountOne = new SavingsAccount("sa1", 1000, 5, 0.1);
        Account accountTwo = new CurrentAccount("sa2", 2000, 2, 0.07);

//        accountOne.deposit(300); // These are type compatible but deposit() is not in Account
        ((SavingsAccount) accountOne).deposit(300);
        accountOne.printBalance();
        accountOne.printInterestEarn();
    }
}
