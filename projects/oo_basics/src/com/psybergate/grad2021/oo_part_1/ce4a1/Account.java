package com.psybergate.grad2021.oo_part_1.ce4a1;

public class Account {

    private String accName;

    protected double balance;

    protected int period;

    protected double interest;

    public Account(String accName, double balance, int period, double interest) {
        this.accName = accName;
        this.balance = balance;
        this.period = period;
        this.interest = interest;
    }

    public double getInterestEarned() {
        return (balance * interest * period);
    }

    public void printBalance() {
        System.out.println("((deposit * interest * period) + deposit) = " + (getInterestEarned() + balance));
    }

    public void printInterestEarn(){
        System.out.println("getInterestEarned() = " + getInterestEarned());
    }
}
