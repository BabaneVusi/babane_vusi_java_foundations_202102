package com.psybergate.grad2021.oo_part_1.ce4a1;

public class SavingsAccount extends Account{

    public SavingsAccount(String accName, double balance, int period, double interest) {
        super(accName, balance, period, interest);
    }

    public void deposit(double amount){
        balance += amount;
    }
}
