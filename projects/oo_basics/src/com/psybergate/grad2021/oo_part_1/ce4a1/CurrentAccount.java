package com.psybergate.grad2021.oo_part_1.ce4a1;

public class CurrentAccount extends Account {

    public CurrentAccount(String accName, double balance, int period, double interest) {
        super(accName, balance, period, interest);
    }

    public void printInterestEarned() {
        System.out.println("(deposit * interest * period) = " + (balance * interest * period) + (0.01 * balance));
    }

    @Override
    public void printBalance() {
        System.out.println("(getInterestEarned() + (0.01 * balance) + balance) = " + (getInterestEarned() + (0.01 *
                balance) + balance));
    }
}
