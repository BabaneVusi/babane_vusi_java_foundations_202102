package com.psybergate.grad2021.oo_part_1.ce4a2;

public class SavingsAccount extends Account {

    private final double ZERO = 0.0;

    private final double MIN_BALANCE = 100000;

    private double interestRate;

    public SavingsAccount(String accountNum, String name, String surname, double balance) {
        super(accountNum, name, surname, balance);
    }

    @Override
    public boolean needsToBeReviewed() {
        if (balance < 2000) {
            return true;
        }
        return false;
    }

    @Override
    public void isOverdrawn() {
        if (balance < MIN_BALANCE) {
            System.out.println("Account is overdrawn");
            return;
        }
        System.out.println("Account is not overdrawn");
    }

    @Override
    public void printBalance() {
        System.out.println("balance = " + balance);
    }

    @Override
    public void withdraw(double amount) {
        if ((balance - amount) > ZERO) {
            balance -= amount;
            System.out.println(amount + " successfully withdrawn");
            System.out.println("Account balance = " + balance);
        } else {
            System.out.println("Withdrawal failed, not enough balance for withdrawal");
        }
    }
}
