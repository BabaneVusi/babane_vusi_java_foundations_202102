package com.psybergate.grad2021.oo_part_1.ce4a2;

import java.util.ArrayList;
import java.util.List;

public class Utils {

    public static void main(String[] args) {
        Account accOne = new SavingsAccount("sa1", "John", "Murry", 8000);
        Account accTwo = new SavingsAccount("sa2", "Jonny", "Jackson", 1000);
        Account accThree = new CurrentAccount("ca1", "Blake", "Jackson", -25000);
        Account accFour = new CurrentAccount("ca2", "Jonny", "Jackson", 1000);
        Account accFive = new CurrentAccount("ca3", "Jonny", "Jackson", -31000);

        List<Account> list = new ArrayList();
        list.add(accOne);
        list.add(accTwo);
        list.add(accThree);
        list.add(accFour);
        list.add(accFive);

        printAccount(list);
    }

    private static void printAccount(List<Account> list) {
        for (Account account : list) {
            System.out.println("account " + account.getAccountNum() + " needsToBeReviewed() = " + account.needsToBeReviewed());
        }
    }
}
