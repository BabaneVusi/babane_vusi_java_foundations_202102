package com.psybergate.grad2021.oo_part_1.ce4a2;

public abstract class Account {

    private String accountNum;

    private String name;

    private String surname;

    protected double balance;

    public String getAccountNum() {
        return accountNum;
    }

    public Account(String accountNum, String name, String surname, double balance) {
        this.accountNum = accountNum;
        this.name = name;
        this.surname = surname;
        this.balance = balance;
    }

    public abstract boolean needsToBeReviewed();

    public abstract void isOverdrawn();

    public abstract void printBalance();

    public abstract void withdraw(double amount);

    public void getAccountType() {

    }

}
