package com.psybergate.grad2021.oo_part_1.Polymorphism;

public class Rubber extends Stationery {

    public Rubber(String name, int units, double price) {
        super(name, units, price);
    }
}
