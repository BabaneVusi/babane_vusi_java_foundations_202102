package com.psybergate.grad2021.oo_part_1.Polymorphism;

public class Stationery {

    private final String name;

    protected final int units;

    protected final double price;

    public Stationery(String name, int units, double price) {
        this.name = name;
        this.units = units;
        this.price = price;
    }

    public double cost() {
        return (units * price);
    }

    public void print() {
        System.out.println("The " + name + " cost() = " + cost());
    }
}
