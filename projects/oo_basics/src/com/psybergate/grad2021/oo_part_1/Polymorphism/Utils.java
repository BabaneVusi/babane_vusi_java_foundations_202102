package com.psybergate.grad2021.oo_part_1.Polymorphism;

import java.util.LinkedList;
import java.util.List;

public class Utils {

    public static void main(String[] args) {
//        Stationery rubber = new Rubber("rubber", 5,5.5);
//        rubber.print();
//
//        Stationery pen = new Pen("pen", 2,7.5);
//        pen.print();

        List<Stationery> list = new LinkedList<Stationery>();
        list.add(new Pen("pens", 2,7.5));
        list.add(new Rubber("rubbers", 5,5.5));

        for (Stationery stationery : list){
            stationery.print();
        }
    }
}
