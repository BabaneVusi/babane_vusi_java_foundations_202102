package com.psybergate.grad2021.oo_part_1.Polymorphism;

public class Pen extends Stationery {

    public Pen(String name, int units, double price) {
        super(name, units, price);
    }

    public double cost() {
        if (units > 3){
        return (units * price * 0.9);
        }
        return (units * price);
    }
}
