package com.psybergate.grad2021.oo_part_1.Inheritance;

public class Utils {

    public static void main(String[] args) {
        Rubber rubber = new Rubber(3.5, 16);
        System.out.println("rubber.cost() = " + rubber.cost());
        System.out.println("rubber.discountedCost() = " + rubber.discountedCost());

        Pen pen = new Pen(5, 5);
        System.out.println("pen.cost() = " + pen.cost());
        System.out.println("pen.discountedCost() = " + pen.discountedCost());
    }
}
