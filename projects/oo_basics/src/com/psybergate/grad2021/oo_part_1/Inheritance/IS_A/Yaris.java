package com.psybergate.grad2021.oo_part_1.Inheritance.IS_A;

public class Yaris extends Car{

    public Yaris(String name, int regNumber, int tankSize, int topSpeed) {
        super(name, regNumber, tankSize, topSpeed);
    }
}
