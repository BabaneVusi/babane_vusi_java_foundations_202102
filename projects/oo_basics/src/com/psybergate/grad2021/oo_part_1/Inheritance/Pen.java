package com.psybergate.grad2021.oo_part_1.Inheritance;

public class Pen extends Stationery{

    public Pen(double price, int units) {
        super(price, units);
    }

    public double discountedCost(){
        if (getUnits() >= 5){
            return (cost() * 0.93);
        }
        return cost();
    }
}
