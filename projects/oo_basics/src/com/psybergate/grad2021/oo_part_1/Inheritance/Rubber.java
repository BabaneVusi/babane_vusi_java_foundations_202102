package com.psybergate.grad2021.oo_part_1.Inheritance;

public class Rubber extends Stationery {

    public Rubber(double price, int units) {
        super(price, units);
    }

    public double discountedCost(){
        if (getUnits() > 15 ){
            return (cost() * 0.9);
        }
        return cost();
    }
}
