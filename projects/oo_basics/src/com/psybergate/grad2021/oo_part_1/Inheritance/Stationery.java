package com.psybergate.grad2021.oo_part_1.Inheritance;

public class Stationery {
    private double price;

    private int units;

    public Stationery(double price, int units) {
        this.price = price;
        this.units = units;
    }

    public double cost() {
        return (price * units);
    }

    public double getPrice() {
        return price;
    }

    public int getUnits() {
        return units;
    }
}
