package com.psybergate.grad2021.oo_part_1.Inheritance.IS_A;

public class Car {

    private final String name;

    private final int regNumber;

    private final int tankSize;

    private final int topSpeed;

    public Car(String name, int regNumber, int tankSize, int topSpeed) {
        this.name = name;
        this.regNumber = regNumber;
        this.tankSize = tankSize;
        this.topSpeed = topSpeed;
    }

    public int travelDistance() {
        return (tankSize - topSpeed);
    }
}
