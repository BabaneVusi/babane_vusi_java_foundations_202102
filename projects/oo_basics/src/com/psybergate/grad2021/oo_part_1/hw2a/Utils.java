package com.psybergate.grad2021.oo_part_1.hw2a;

public class Utils {

    public static void main(String[] args) {
        MyDatePP pp = new MyDatePP();
        pp.setReferenceDate(20210101);
        pp.printDate(3);
        pp.printDate(30);
        pp.printDate(365);

        System.out.println("\n");

        MyDateOO oo = new MyDateOO(20210101);
        oo.addDay(3);
        oo.printDate();
        oo.addDay(30);
        oo.printDate();
        oo.addDay(365);
        oo.printDate();
    }
}
