package com.psybergate.grad2021.oo_part_1.ce3b;

public class Pen extends Stationery {

    private double discount;

    public Pen(String name, int units, double price, double discount) {
        super(name, units, price);
        this.discount = discount;
    }

    public double cost() {
        return (getUnits() * getPrice() * (1 - discount));
    }

    public void printCost() {
        if (getUnits() > 10.0) {
            System.out.println("discounted cost() = " + this.cost());
            return;
        }
            System.out.println("cost() = " + super.cost());
    }
}
