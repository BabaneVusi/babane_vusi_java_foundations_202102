package com.psybergate.grad2021.oo_part_1.ce3b;

public class Utils {

    public static void main(String[] args) {
        Stationery stationery = new Pen("Big", 2, 2, 0.1);
        ((Pen) stationery).printCost();
        Stationery stationery1 = new Pen("Big", 12, 3, 0.1);
        ((Pen) stationery1).printCost();
    }
}
