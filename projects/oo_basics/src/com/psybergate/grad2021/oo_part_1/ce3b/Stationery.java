package com.psybergate.grad2021.oo_part_1.ce3b;

public class Stationery {

    private String name;

    private int units;

    private double price;

    public Stationery(String name, int units, double price) {
        this.name = name;
        this.units = units;
        this.price = price;
    }

    public double cost(){
        return (units * units);
    }

    public double getPrice() {
        return price;
    }

    public int getUnits() {
        return units;
    }
}
