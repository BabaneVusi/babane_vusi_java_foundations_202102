package com.psybergate.grad2021.oo_part_1.Encapsulation;

public class Pencil extends Stationery {

    public Pencil(String name, int price, int units) {
        super(name, price, units);
    }

    public double discountCost() {
        return cost() * 0.9;
    }
}
