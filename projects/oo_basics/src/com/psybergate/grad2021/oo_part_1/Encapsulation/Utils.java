package com.psybergate.grad2021.oo_part_1.Encapsulation;

public class Utils {

    public static void main(String[] args) {
        Stationery stationery = new Pencil("HB", 2, 6);
        System.out.println("car.printSpeed() = " + stationery.cost());
        System.out.println("((Pencil) stationery).discountCost() = " + ((Pencil) stationery).discountCost());
    }
}
