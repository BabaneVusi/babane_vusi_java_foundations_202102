package com.psybergate.grad2021.oo_part_1.Encapsulation;

public class Stationery {
    private String name;

    private int price;

    private int units;

    public Stationery(String name, int price, int units) {
        this.name = name;
        this.price = price;
        this.units = units;
    }

    public double cost() {
        return price * units;
    }
}
