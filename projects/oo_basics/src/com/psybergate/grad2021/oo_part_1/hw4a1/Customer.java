package com.psybergate.grad2021.oo_part_1.hw4a1;

public class Customer {

    private static final int MINIMUM_AGE = 18;

    private static final int MAX_INSTALMENT_PERIOD = 12;

    private String customerNum;

    private String itemName;

    private int customerAge;

    private double deposit;

    private double price;

    public Customer(String customerNum, String itemName, int deposit, int price) {
        this.customerNum = customerNum;
        this.itemName = itemName;
        this.deposit = deposit;
        this.price = price;
    }

    public Customer(String customerNum, String itemName, int deposit, int price, int customerAge) {
        this(customerNum, itemName, deposit, price);
        if (isAgeValid(customerAge) == false) {
            throw new RuntimeException("Customer age invalid");
        }
        this.customerAge = customerAge;
    }

    private static boolean isAgeValid(int age) {
        return (age > MINIMUM_AGE);
    }

    public double getYearInstalments() {
        return (outstandingBalance() / MAX_INSTALMENT_PERIOD);
    }

    private double outstandingBalance() {
        return (price - deposit);
    }
}
