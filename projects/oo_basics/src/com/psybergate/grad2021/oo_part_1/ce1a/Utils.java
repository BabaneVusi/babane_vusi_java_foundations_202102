package com.psybergate.grad2021.oo_part_1.ce1a;

public class Utils {

    public static void main(String[] args) {
        Customer customer1 = new Customer("a1", "Vusi", "Rondebult");
        customer1.addCurrentAccount(new CurrentAccount("ca1",1200));
        customer1.addCurrentAccount(new CurrentAccount("ca2",1500));

        customer1.printBalance();
    }
}
