package com.psybergate.grad2021.oo_part_1.ce1a;

public class CurrentAccount {

    private String accountNum;

    private double balance;

    public CurrentAccount(String accountNum, double balance) {
        this.accountNum = accountNum;
        this.balance = balance;
    }

    public double getBalance() {
        return balance;
    }
}
