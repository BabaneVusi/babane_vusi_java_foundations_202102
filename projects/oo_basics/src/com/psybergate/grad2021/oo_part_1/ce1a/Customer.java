package com.psybergate.grad2021.oo_part_1.ce1a;

import java.util.ArrayList;
import java.util.List;

public class Customer {

    private String customerNum;

    private String name;

    private String address;

    List<CurrentAccount> list = new ArrayList();

    public Customer(String customerNum, String name, String address) {
        this.customerNum = customerNum;
        this.name = name;
        this.address = address;
    }

    public void addCurrentAccount(CurrentAccount account) {
        list.add(account);
    }

    private double calculateBalance() {
        double sum = 0.0;
        for (CurrentAccount currentAccount : list) {
            sum += currentAccount.getBalance();
        }
        return sum;
    }

    public void printBalance(){
        System.out.println(customerNum + " balance is = " + calculateBalance());
    }
}
