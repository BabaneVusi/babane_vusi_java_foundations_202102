package com.psybergate.grad2021.oo_part_1.ce2a3a;

import java.util.ArrayList;
import java.util.List;

public class TestClass {

    public static void main(String[] args) {
        // ce2a
        CurrentAccount curAccOne = new CurrentAccount("an1", "Vusi", "Ngobe", 1000);
        CurrentAccount curAccTwo = new CurrentAccount("an2", "Vuyo", "Ncube", 2300);
        CurrentAccount curAccThree = new CurrentAccount("an3", "Sphe", "Nqge", 3300);

        curAccOne.withdraw(2500);
        curAccOne.isOverdrawn();
        System.out.println("curAccONe.needsToBeReviewed() = " + curAccOne.needsToBeReviewed() + "\n");

        curAccTwo.withdraw(28000);
        curAccTwo.isOverdrawn();
        System.out.println("curAccTwo.needsToBeReviewed() = " + curAccTwo.needsToBeReviewed() + "\n");

        curAccThree.withdraw(200);
        curAccThree.isOverdrawn();
        System.out.println("curAccThree.needsToBeReviewed() = " + curAccThree.needsToBeReviewed() + "\n");

        //  ce3a
        Account account = new CurrentAccount("an4", "Nic", "Williamson", 300);
        List<CurrentAccount> list = new ArrayList();
        list.add((CurrentAccount) account);

        CurrentAccount curAcc = list.get(0);
        curAcc.withdraw(50);
        list.add(curAcc);

        ((CurrentAccount) account).printBalance();
        curAcc.printBalance();
    }
}
