package com.psybergate.grad2021.oo_part_1.ce2a3a;

public class CurrentAccount extends Account {

    private final double ZERO = 0.0;

    private final double MAX_OVERDRAFT = 100000;

    private double interestRate;

    public double overdraft;

    public CurrentAccount(String accountNum, String name, String surname, double balance) {
        super(accountNum, name, surname, balance);
    }

    @Override
    public boolean needsToBeReviewed() {
        if (balance < (0.2 * (-1 * MAX_OVERDRAFT))) {
            return true;
        }
        return false;
    }

    public void isOverdrawn() {
        if (balance < ZERO) {
            System.out.println("Account is overdrawn");
            return;
        }
        System.out.println("Account is not overdrawn");
    }

    public void getAccountType() {

    }

    public void printBalance() {
        System.out.println("balance = " + balance);
    }

    public void withdraw(double amount) {
        if (balance >= (-1 * MAX_OVERDRAFT)) {
            balance -= amount;
            System.out.println(amount + " successfully withdrawn");
            System.out.println("Account balance = " + balance);
        } else {
            System.out.println("Withdraw failed, Max overdraft exceeded");
        }
    }
}
