package com.psybergate.grad2021.oo_part_1.ce2a3a;

public class Account {

    private String accountNum;

    private String name;

    private String surname;

    protected double balance;

    public Account(String accountNum, String name, String surname, double balance) {
        this.accountNum = accountNum;
        this.name = name;
        this.surname = surname;
        this.balance = balance;
    }

    public boolean needsToBeReviewed(){
        if (balance <= -500000){
            return true;
        }
        return false;
    }

    public void getAccountType(){

    }
}
