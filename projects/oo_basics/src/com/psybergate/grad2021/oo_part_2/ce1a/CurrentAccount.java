package com.psybergate.grad2021.oo_part_2.ce1a;

public class CurrentAccount extends Account {

    private static final int MAX_OVERDRAFT = 2500;

    private int overdraft;

    public CurrentAccount(String accountName, int accountNumber, int overdraft) {
        super(accountName, accountNumber);
        this.overdraft = overdraft;
        System.out.println("accountName and accountNumber = " + accountName + " & " + accountNumber + ", overdraft = "
                + overdraft);
    }

//    public Account(String accountName, int accountNumber){
//        this.accountName = accountName;
//        this.accountNumber = accountNumber;
//    }
}
