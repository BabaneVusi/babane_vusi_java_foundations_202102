package com.psybergate.grad2021.oo_part_2.ce1a;

public class Account {

    private String accountName;

    private int accountNumber;

    public Account(String accountName, int accountNumber) {
        this.accountName = accountName;
        this.accountNumber = accountNumber;

        System.out.println("accountName = " + accountName);
        System.out.println("accountNumber = " + accountNumber);
    }
}
