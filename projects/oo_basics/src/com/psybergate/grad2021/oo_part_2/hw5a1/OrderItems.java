package com.psybergate.grad2021.oo_part_2.hw5a1;

public class OrderItems {

    private String itemName;

    private double itemPrice;

    public OrderItems(String itemName, double itemPrice) {
        this.itemName = itemName;
        this.itemPrice = itemPrice;
    }

    public double getItemPrice() {
        return itemPrice;
    }
}
