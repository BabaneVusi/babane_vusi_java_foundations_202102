package com.psybergate.grad2021.oo_part_2.hw5a1;

public class OrderUtils {
    public static void main(String[] args) {
        Customer customer0 = new Customer("Vusi00");
        Order order0 = new InternationalOrder("00");
        order0.addItems(new OrderItems("Banana", 3));
        order0.addItems(new OrderItems("Apple", 6));
        order0.addItems(new OrderItems("Doritos", 36));

        Customer customer1 = new Customer("Vuyo01");
        Order order1 = new LocalOrder("01");
        order1.addItems(new OrderItems("Banana", 3));
        order1.addItems(new OrderItems("Sandwich", 40));
        order1.addItems(new OrderItems("Apple", 6));
        order1.addItems(new OrderItems("Orange", 8));

        Customer customer2 = new Customer("Jack02");
        Order order2 = new InternationalOrder("02");
        order2.addItems(new OrderItems("Polish", 23));
        order2.addItems(new OrderItems("WashingPowder", 40));
        order2.addItems(new OrderItems("Dettol", 65));
        order2.addItems(new OrderItems("Pie", 18));

        Customer customer3 = new Customer("Ricky03");
        Order order3 = new LocalOrder("00");
        order3.addItems(new OrderItems("Banana", 3));
        order3.addItems(new OrderItems("Apple", 6));
        order3.addItems(new OrderItems("Doritos", 36));

        customer0.addOrder(order0);
        customer1.addOrder(order1);
        customer2.addOrder(order2);
        customer3.addOrder(order3);

        System.out.println("customer0.calculateTotalCost() = " + customer0.calculateTotalCost());
        System.out.println("customer1.calculateTotalCost() = " + customer1.calculateTotalCost());
        System.out.println("customer2.calculateTotalCost() = " + customer2.calculateTotalCost());
        System.out.println("customer3.calculateTotalCost() = " + customer3.calculateTotalCost());
    }
}
