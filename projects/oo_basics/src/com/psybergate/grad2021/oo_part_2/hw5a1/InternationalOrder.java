package com.psybergate.grad2021.oo_part_2.hw5a1;

public class InternationalOrder extends Order {

    private static final double IMPORT_DUTIES = 0.12;

    public InternationalOrder(String orderID) {
        super(orderID);
    }

    @Override
    public double getOrderTotal() {
        return (super.getOrderTotal() + getImportCost());
    }

    private double getImportCost() {
        return super.getOrderTotal() * IMPORT_DUTIES;
    }
}
