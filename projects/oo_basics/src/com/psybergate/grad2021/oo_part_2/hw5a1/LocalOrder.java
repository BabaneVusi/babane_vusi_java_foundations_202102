package com.psybergate.grad2021.oo_part_2.hw5a1;

public class LocalOrder extends Order {

    private static final double DISCOUNT = 0.89;

    public LocalOrder(String orderID) {
        super(orderID);
    }

    @Override
    public double getOrderTotal() {
        return (super.getOrderTotal() * DISCOUNT);
    }
}
