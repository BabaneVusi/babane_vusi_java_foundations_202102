package com.psybergate.grad2021.oo_part_2.hw5a1;

import java.util.ArrayList;
import java.util.List;

public class Order {

    private String orderID;

    private List<OrderItems> list = new ArrayList<>();

    public Order(String orderID) {
        this.orderID = orderID;
    }

    public void addItems(OrderItems objectRef){
        list.add(objectRef);
    }

    public double getOrderTotal() {
        double sumValue = 0;
        for (OrderItems orderItem : list) {
            sumValue += orderItem.getItemPrice();
        }
        return sumValue;
    }
}
