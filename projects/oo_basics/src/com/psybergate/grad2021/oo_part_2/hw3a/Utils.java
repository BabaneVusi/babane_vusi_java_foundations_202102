package com.psybergate.grad2021.oo_part_2.hw3a;

public class Utils {

    public static void main(String[] args) {
        Customer customer = new Customer("MachineTrading", "sewing Machine", 7000,
                25000);

        Customer customer1 = new Customer("MachineTrading", "sewing Machine", 10000,
                45000);

        Customer customer2 = customer1;

        System.out.println("customer.isEqual(customer1) = " + customer.isEqual(customer1));
        customer2.updateCustomerName("SontongaTrading");
        System.out.println("customer.isEqual(customer1) = " + customer1.isIdentical(customer2));

        System.out.println("customer.isEqual(customer2) = " + customer.isEqual(customer2));
        System.out.println("customer.isIdentical(customer1) = " + customer.isIdentical(customer1));
    }
}
