package com.psybergate.grad2021.oo_part_2.hw4a;

public class Rectangle extends Shape {

    private int length;

    private int width;

    public Rectangle(int length, int width) {
        this.length = length;
        this.width = width;
    }

    @Override
    public int calculateArea() {
        return (length * width);
    }

    public static boolean isAreaValid() {
        return true;
    }
}
