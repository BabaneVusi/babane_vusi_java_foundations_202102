package com.psybergate.grad2021.oo_part_2.hw4a;

public class Utils {

    public static void main(String[] args) {
        Rectangle rectangle = new Rectangle(4, 3);
        System.out.println("rectangle.calculateArea() = " + rectangle.calculateArea());
        System.out.println("rectangle.getMaximumArea() = " + rectangle.getMaximumArea());
        System.out.println("rectangle.isAreaValid() = " + rectangle.isAreaValid());
    }
}
