package com.psybergate.grad2021.oo_part_2.hw4a;

public class Shape {

    private static final int MAXIMUM_AREA = 2000;

    public int calculateArea() {
        return 0;
    }

    public static boolean isAreaValid() {
        return false;
    }

    public static int getMaximumArea() {
        return MAXIMUM_AREA;
    }
}
