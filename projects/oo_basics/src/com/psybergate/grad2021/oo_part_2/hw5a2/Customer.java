package com.psybergate.grad2021.oo_part_2.hw5a2;

import java.util.ArrayList;
import java.util.List;

public class Customer {

    private String customerID;

    List<Order> orderItems = new ArrayList<>();

    public Customer(String customerID) {
        this.customerID = customerID;
    }

    public void addOrder(Order order) {
        orderItems.add(order);
    }

    public double calculateTotalCost() {
        double sumValue = 0;
        for (Order order : orderItems) {
            sumValue += order.calculateOrderCost();
        }
        return sumValue;
    }
}
