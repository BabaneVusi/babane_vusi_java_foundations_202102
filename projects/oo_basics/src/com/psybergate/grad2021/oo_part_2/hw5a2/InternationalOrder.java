package com.psybergate.grad2021.oo_part_2.hw5a2;

public class InternationalOrder extends Order {

    public static final double IMPORT_DUTIES = 0.12;

    public InternationalOrder(String orderID, String chosenDiscount, double customerNumberOfYears) {
        super("I" + orderID, chosenDiscount, customerNumberOfYears);
    }

    public InternationalOrder(String orderID, String chosenDiscount) {
        super(orderID, chosenDiscount);
    }
}
