package com.psybergate.grad2021.oo_part_2.hw5a2;

public class LocalOrder extends Order {

    public LocalOrder(String orderID, String chosenDiscount, double customerNumberOfYears) {
        super("L" + orderID, chosenDiscount, customerNumberOfYears);
    }

    public LocalOrder(String orderID, String chosenDiscount) {
        super(orderID, chosenDiscount);
    }
}
