package com.psybergate.grad2021.oo_part_2.hw5a2;

import java.util.ArrayList;
import java.util.List;

public class Order {

    private static final double TWO_YEARS = 2;

    private static final double FIVE_YEARS = 5;

    private static final double OVER_TWO_YEARS_DISCOUNT = 0.925;

    private static final double OVER_FIVE_YEARS_DISCOUNT = 0.875;

    private static final double FIVE_HUNDRED_THOUSAND_RANDS = 500000;

    private static final double ONE_MILLION_RANDS = 1000000;

    private static final double OVER_FIVE_HUNDRED_THOUSAND_RANDS_DISCOUNT = 0.95;

    private static final double OVER_ONE_MILLION_RANDS_DISCOUNT = 0.9;

    private String orderID;

    private String chosenDiscount;

    private double customerNumberOfYears;

    private List<OrderItems> list = new ArrayList<>();

    public Order(String orderID, String chosenDiscount, double customerNumberOfYears) {
        this(orderID, chosenDiscount);
        this.customerNumberOfYears = customerNumberOfYears;
    }

    public Order(String orderID, String chosenDiscount) {
        this.orderID = orderID;
        this.chosenDiscount = chosenDiscount;
    }

    public void addItems(OrderItems objectRef) {
        list.add(objectRef);
    }

    public double calculateOrderCost() {
        if (chosenDiscount == "Loyalty") {
            if (orderID.indexOf(0) == 'I') {
                return (loyaltyDiscountedCost(customerNumberOfYears) + getImportCost());
            }
            return loyaltyDiscountedCost(customerNumberOfYears);
        } else if (chosenDiscount == "itemsCost") {
            if (orderID.indexOf(0) == 'I') {
                return (itemsDiscountedCost(getOrderTotalCost()) + getImportCost());
            }
            return itemsDiscountedCost(getOrderTotalCost());
        }

        if (orderID.indexOf(0) == 'I') {
            return getOrderTotalCost() + getImportCost();
        }
        return getOrderTotalCost();
    }

    public double loyaltyDiscountedCost(double customerYears) {
        if (getOrderTotalCost() > TWO_YEARS & getOrderTotalCost() <= FIVE_YEARS) {
            return (getOrderTotalCost() * OVER_TWO_YEARS_DISCOUNT);
        } else if (getOrderTotalCost() > FIVE_YEARS) {
            return (getOrderTotalCost() * OVER_FIVE_YEARS_DISCOUNT);
        }
        return getOrderTotalCost();
    }

    public double itemsDiscountedCost(double itemsCost) {
        if (getOrderTotalCost() > FIVE_HUNDRED_THOUSAND_RANDS & getOrderTotalCost() <= ONE_MILLION_RANDS) {
            return ((getOrderTotalCost() * OVER_FIVE_HUNDRED_THOUSAND_RANDS_DISCOUNT));
        } else if (getOrderTotalCost() >= ONE_MILLION_RANDS) {
            return ((getOrderTotalCost() * OVER_ONE_MILLION_RANDS_DISCOUNT));
        }
        return (getOrderTotalCost());
    }

    public double getOrderTotalCost() {
        double sumValue = 0;
        for (OrderItems orderItem : list) {
            sumValue += orderItem.getItemPrice();
        }
        return sumValue;
    }

    private double getImportCost() {
        return getOrderTotalCost() * InternationalOrder.IMPORT_DUTIES;
    }
}
