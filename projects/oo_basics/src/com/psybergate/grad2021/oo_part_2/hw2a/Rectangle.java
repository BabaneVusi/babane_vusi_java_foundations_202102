package com.psybergate.grad2021.oo_part_2.hw2a;

import java.util.Objects;

public class Rectangle {

    private static final int MAXIMUM_LENGTH = 200;

    private static final int MAXIMUM_WIDTH = 100;

    private static final int MAXIMUM_AREA = 15000;

    private int length;

    private int width;

    public Rectangle(int length, int width) {

        if (isRectangleValid(length, width) == false | isLengthValid(length) == true | isWidthValid(width) == true |
                isAreaValid(length, width) == true) {
            throw new RuntimeException("length not greater than the width!");
        }

        this.length = length;
        this.width = width;
    }

    private static boolean isRectangleValid(int sideA, int sideB) {
        return (sideA > sideB);
    }

    private static boolean isLengthValid(int length) {
        return (length > MAXIMUM_LENGTH);
    }

    private static boolean isWidthValid(int width) {
        return (width > MAXIMUM_WIDTH);
    }

    private boolean isAreaValid(int length, int width) {
        return (getRectangleArea(length, width) > MAXIMUM_WIDTH);
    }

    private int getRectangleArea(int length, int width) {
        return (length * width);
    }

    public void printArea() {
        System.out.println("Rectangle Area = " + getRectangleArea(length, width));
    }

    public boolean isEqual(Rectangle rectangleReference){
        return (this.length == rectangleReference.getLength() && this.width == rectangleReference.getWidth());
    }

    public boolean isIdentical(Rectangle rectangleReference) {
        return (this == rectangleReference);
    }

    public int getLength() {
        return length;
    }

    public int getWidth() {
        return width;
    }
}
