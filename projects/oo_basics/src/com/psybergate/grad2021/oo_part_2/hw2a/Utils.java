package com.psybergate.grad2021.oo_part_2.hw2a;

public class Utils {

    public static void main(String[] args) {
        Rectangle rectangle = new Rectangle(12, 4);
        Rectangle rectangle1 = new Rectangle(12, 4);
        Rectangle rectangle2 = rectangle;

        System.out.println("rectangle.isIdentical(rectangle1) = " + rectangle.isEqual(rectangle1));
        System.out.println("rectangle.isIdentical(rectangle1) = " + rectangle.isIdentical(rectangle1));
        System.out.println("rectangle.isIdentical(rectangle3) = " + rectangle.isIdentical(rectangle2));
    }
}
