package com.psybergate.grad2021.oo_part_2.demo2a;

import java.util.Objects;

public class CurrentAccount {

    private final static double INTEREST = 0.02;

    private String accountName;

    private double balance;

    private int years;

    public CurrentAccount(String accountName, double balance, int years) {
        this.accountName = accountName;
        this.balance = balance;
        this.years = years;
    }

    private double calculateBalance() {
        return (balance * years * INTEREST);
    }

    public void printBalance() {
        System.out.println("balance = " + calculateBalance());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CurrentAccount that = (CurrentAccount) o;
        return Double.compare(that.balance, balance) == 0 && years == that.years && Objects.equals(accountName,
                that.accountName);
    }
}
