package com.psybergate.grad2021.oo_part_2.demo2a;

import java.util.ArrayList;
import java.util.List;

public class Utils {

    public static void main(String[] args) {
        CurrentAccount ca = new CurrentAccount("ca1", 2345, 3);
        CurrentAccount ca1 = new CurrentAccount("ca1", 2345, 3);

        List<CurrentAccount> accounts = new ArrayList<>();
        accounts.add(ca);

        accounts.get(0).printBalance();

        System.out.println(ca == accounts.get(0));
        System.out.println(ca == ca1);
        System.out.println(ca.equals(ca1));
    }
}
