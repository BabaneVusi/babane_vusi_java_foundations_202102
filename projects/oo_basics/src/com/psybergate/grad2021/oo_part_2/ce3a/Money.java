package com.psybergate.grad2021.oo_part_2.ce3a;

import java.math.BigDecimal;

public class Money {

    private int integerValue;

    private double doubleValue;

    BigDecimal bigDecimal;

    public Money(int integerValue) {
        this.integerValue = integerValue;
        bigDecimal = new BigDecimal(integerValue);
    }

    public Money(double doubleValue) {
        this.doubleValue = doubleValue;
        bigDecimal = new BigDecimal(doubleValue);
    }

    public BigDecimal sum(int moneyValue) {
        BigDecimal money = new BigDecimal(moneyValue);
        return (money.add(bigDecimal));
    }

    public BigDecimal sum(double moneyValue) {
        BigDecimal money = new BigDecimal(moneyValue);
        return (money.add(bigDecimal));
    }
}
