package com.psybergate.grad2021.oo_part_2.ce3a;

public class Utils {

    public static void main(String[] args) {
        Money money = new Money(5);
        System.out.println("money.sum(5) = " + money.sum(5));

        Money doubleMoney = new Money(5.5);
        System.out.println("money.sum(5) = " + doubleMoney.sum(5.4));
    }
}
