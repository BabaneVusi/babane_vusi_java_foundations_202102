package com.psybergate.grad2021.oo_part_2.demo1a;

public class Shape {

    private final int ZERO = 0;

    public Shape() {
    }

    public int calculateArea() {
        return ZERO;
    }

    public static boolean validateSides() {
        return false;
    }
}
