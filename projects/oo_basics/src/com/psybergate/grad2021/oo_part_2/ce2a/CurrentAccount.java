package com.psybergate.grad2021.oo_part_2.ce2a;

public class CurrentAccount {


    private final static double INTEREST = 0.02;

    private String accountName;

    private double balance;

    private int years;

    public CurrentAccount(String accountName, double balance, int years) {
        this.accountName = accountName;
        this.balance = balance;
        this.years = years;
    }

    private double calculateBalance() {
        return (balance * years * INTEREST);
    }

    public void printBalance() {
        System.out.println("balance = " + calculateBalance());
    }
}
