package com.psybergate.grad2021.oo_part_2.ce2a;

public class Utils {

    public static void main(String[] args) {
        CurrentAccount ca = new CurrentAccount("ca", 4567, 5);
        CurrentAccount ca1 = new CurrentAccount("ca1", 1000, 5);

        ca.printBalance();
        ca1.printBalance();

//        // Swapping objects
//        CurrentAccount tempReference = ca;
//        ca = ca1;
//        ca1 = tempReference;

        swap(ca, ca1);

        ca.printBalance();
        ca1.printBalance();
    }

    private static void swap(CurrentAccount ca, CurrentAccount ca1) {
        CurrentAccount tempObjectRef = ca;
        ca = ca1;
        ca1 = tempObjectRef;
    }
}
