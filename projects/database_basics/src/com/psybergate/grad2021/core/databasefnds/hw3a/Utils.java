package com.psybergate.grad2021.core.databasefnds.hw3a;

import java.io.FileWriter;
import java.io.IOException;
import java.sql.SQLException;

import static com.psybergate.grad2021.core.databasefnds.hw3a.DatabaseManipulation.getSqlQuery;

public class Utils {

    public static void main(String[] args) throws SQLException {
        writeToFile();
    }

    public static void writeToFile() throws SQLException {
        try {
            FileWriter writeData = new FileWriter("src\\com\\psybergate\\grad2021\\core\\databases\\hw3a\\DML.sql");
            writeData.write(getSqlQuery());
            writeData.close();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
