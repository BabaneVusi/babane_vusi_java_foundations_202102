package com.psybergate.grad2021.core.databasefnds.hw3a;

import java.sql.SQLException;

public class DatabaseManipulation {

    public static String getSqlQuery() throws SQLException {
        String sql =
                addColumn() + updateReviewedColumn() + deleteLowestBalanceAccount() + currentAccountsView() +
                        customerAccounts() + getJohannesburgCustomers();
        return sql;
    }

    /**
     * Add a column on a table
     *
     * @return
     */
    public static String addColumn() {
        String sqlQuery = "alter table accounts add needsToBeReviewed boolean not null default false;";
        return sqlQuery;
    }

    public static String updateReviewedColumn() throws SQLException {
        String sqlQuery = "update accounts set needsToBeReviewed = true where balance < 2000;";
        return sqlQuery;
    }

    /**
     * Deletes an account with the lowest balance
     *
     * @return
     */
    public static String deleteLowestBalanceAccount() {
        String sqlQuery = "delete from accounts where balance = (select min(balance) from accounts);";
        return sqlQuery;
    }

    public static String currentAccountsView() {
        String sqlQuery = "create view current_accounts as select accountnum, accounttypeid, balance, customernum, " +
                "needstobereviewed from accounts where accounttypeid = 3;";
        sqlQuery += "select * from current_accounts;";
        return sqlQuery;
    }

    /**
     * Shows all the accounts of the specified customer.
     *
     * @return
     */
    public static String customerAccounts() {
        String sqlQuery = "select accountnum, accounttypeid, balance, customernum, needstobereviewed from accounts " +
                "where customernum = 20;";
        return sqlQuery;
    }

    /**
     * Displays the customers and accounts of all customers who live in Johannesburg
     * @return
     */
    public static String getJohannesburgCustomers() {
        String sqlQuery = "select * from customers as t1 join accounts as t2 on t1.customernum = t2.customernum" +
                " where address = 'Durban';";
        return sqlQuery;
    }

    public static String transactionsWithSpecificBalance() {
        String sqlQuery = "select * from transactions as t1 join accounts as t2 on t1.accountnum = t2.accountnum where" +
                " balance > 6000;";
        return sqlQuery;
    }

    /**
     * This method shows the usage of a full outer join
     * @return
     */
    public static String fullOuterJonDisplay() {
        String sqlQuery = "select * from customers as t1 full outer join accounts as t2 on t1.customernum = t2" +
                ".customernum where address = 'Durban';";
        return sqlQuery;
    }

    /**
     * Search and displays customers using the first character of customers name
     * @return
     */
    public static String getCustomersWithFirstNameCharacter() {
        String sqlQuery = "select * from customers where name LIKE 'y%';";
        return sqlQuery;
    }

    /**
     * Shows the number of accounts of each customer and sum of all account balances
     * @return
     */
    public static String getAccountsAndBalanceSum() {
        String sqlQuery = "select name, count(accountnum), sum(balance) from customers, accounts where " +
                "customers.customernum = accounts.customernum group by name;";
        return sqlQuery;
    }

    /**
     * Display customers with the specified number of accounts and sum total of the accounts balance
     * @return
     */
    public static String getCustomerWithAccountsAndBalance() {
        String sqlQuery = "select name, count(accountnum), sum(balance) from customers, accounts where " +
                "customers.customernum = accounts.customernum group by name having count(accountnum) > 5 and " +
                "sum(balance) > 1000;";
        return sqlQuery;
    }

    /**
     * Shows the minimum and maximum balance of accounts for each customer
     * @return
     */
    public static String displayMinAndMaxBalance() {
        String sqlQuery = "select name, min(balance), max(balance) from customers as t1, accounts as t2 where " +
                "t1.customernum = t2.customernum group by name;";
        return sqlQuery;
    }

    /**
     * Displays customers with minimum and maximum balance that are within the specified min, max balance range
     * @return
     */
    public static String displayBalanceBtwn1000And8000() {
        String sqlQuery = "select name, min(balance), max(balance) from customers as t1, accounts as t2 where " +
                "t1.customernum = t2.customernum group by name having min(balance) > 1000 and max(balance) < 8000;";
        return sqlQuery;
    }
}