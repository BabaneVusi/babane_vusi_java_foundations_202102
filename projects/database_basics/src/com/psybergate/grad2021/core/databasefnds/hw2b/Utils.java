package com.psybergate.grad2021.core.databasefnds.hw2b;

import java.sql.Connection;
import java.sql.SQLException;

import static com.psybergate.grad2021.core.databasefnds.hw2b.DatabaseConnection.createConnection;
import static com.psybergate.grad2021.core.databasefnds.hw2b.DatabaseTable.*;

public class Utils {

    public static void main(String[] args) throws SQLException {
        Connection connection = createConnection();
        customerTable(connection);
        accountsTable(connection);
        transactionTable(connection);
    }
}
