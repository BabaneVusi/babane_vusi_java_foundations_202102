package com.psybergate.grad2021.core.databasefnds.hw3a;

import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class Table {

    public static String insertData() {
        String sql =
                insertCustomers() + insertAccountTypes() + insertAccounts() + insertTransactionTypes() +
         insertTransactions();
        return sql;
    }

    public static String createTables() {
        String sql =
                customerTableQuery() + accountTypesQuery() + accountsTableQuery() + transactionTypesQuery() +
                        transactionsTableQuery();
        return sql;
    }

    public static String customerTableQuery() {
        String sqlQuery = "create table if not exists customers (customerNum serial primary key, name varchar, " +
                "surname varchar, startDate date, address varchar);";
        return sqlQuery;
    }

    public static String accountsTableQuery() {
        String sqlQuery = "create table if not exists accounts (accountNum serial primary key, accountTypeID int," +
                "balance int, " +
                "customerNum int, constraint fk_01 foreign key (customerNum) references customers on delete " +
                "cascade," +
                "constraint fk_02 foreign key (accountTypeID) references accountTypes on delete cascade);";
        return sqlQuery;
    }

    public static String transactionsTableQuery() {
        String sqlQuery = "create table if not exists transactions (transactionNum serial primary key, accountNum " +
                "int, " +
                "transactionID int, transDate date, constraint fk_03 foreign key (accountNum) references accounts " +
                "on " +
                "delete cascade, constraint fk_04 foreign key (transactionID) references transactionTypes on delete" +
                " " +
                "cascade);";
        return sqlQuery;
    }

    public static String transactionTypesQuery() {
        String sqlQuery = "create table if not exists transactionTypes (transactionID serial primary " +
                "key, transactionType varchar);";
        return sqlQuery;
    }

    public static String accountTypesQuery() {
        String sqlQuery = "create table if not exists accountTypes (accountTypeID serial primary key, accountType " +
                "varchar);";
        return sqlQuery;
    }

    public static String insertCustomers() {
        List<String> addresses = Arrays.asList("Johannesburg", "Cape Town", "Parktown", "Soweto", "Durban", "Port " +
                "Elizabeth", "Germiston", "Midrand");
        String sqlQuery = "insert into customers values";
        String name = "vusi";
        String surname = "babane";
        for (int i = 0; i < 20; i++) {
            sqlQuery += " (DEFAULT, '" + name + i + "', '" + surname + i + "', '" + randNG(1960, 2000) + "-" +
                    randNG(1, 12) + "-" + randNG(1, 28) + "', '" + addresses.get(randNG(0, 7)) + "')";
            if (i < 19) {
                sqlQuery += ",";
            }
        }
        return sqlQuery + ";";
    }

    public static String insertAccountTypes() {
        String sqlQuery = "insert into accountTypes values (DEFAULT, 'current'), (DEFAULT, 'savings'), ( DEFAULT, " +
                "'creditcard');";
        return sqlQuery;
    }

    public static String insertAccounts() {
        String sqlQuery = "insert into accounts values";
        for (int i = 1; i <= 20; i++) {
            int accounts = randNG(0, 8);
            for (int j = 1; j <= accounts; j++) {
                sqlQuery += "(DEFAULT, '" + randNG(1, 3) + "', '" + randNG(-6345, 9876) + "', '" + i + "')";

                if (i < 20 || j < accounts) {
                    sqlQuery += ",";
                }
            }
        }
        return sqlQuery + ";";
    }

    public static String insertTransactionTypes() {
        String sqlQuery = "insert into transactionTypes values (DEFAULT, 'deposit'), (DEFAULT, 'withdraw');";
        return sqlQuery;
    }

    public static String insertTransactions() {
        String sqlQuery = "insert into transactions values";
        for (int i = 1; i <= 94; i++) {
            int transactions = randNG(0, 3);
            for (int j = 1; j <= transactions; j++) {
                sqlQuery += "(DEFAULT, '" + i + "', '" + randNG(1, 2) + "', '" + randNG(1960, 2000) + "-" +
                        randNG(1, 12) + "-" + randNG(1, 28) + "' )";

                if (i < 94 || j < transactions) {
                    sqlQuery += ",";
                }
            }
        }
        return sqlQuery + ";";
    }

    public static int randNG(int min, int max) {
        Random random = new Random();
        return random.nextInt((max - min) + 1) + min;
    }
}