package com.psybergate.grad2021.core.databasefnds.hw3a;

import java.sql.Connection;
import java.sql.DriverManager;

public class DatabaseConnection {

    public static final String USER = "postgres";
    public static final String PASSWORD = "admin";
    public static final String DATABASE_URL = "jdbc:postgresql://localhost:5432/customer_info";
    public static final String DRIVER_URL = "org.postgresql.Driver";

    public static Connection createConnection() {
        Connection connection = null;
        try {
            Class.forName(DRIVER_URL);
            connection = DriverManager
                    .getConnection(DATABASE_URL,
                            USER, PASSWORD);
            System.out.println("Opened database successfully");
        } catch (Exception e) {
            e.printStackTrace();
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
            System.exit(0);
        }
        return connection;
    }
}
