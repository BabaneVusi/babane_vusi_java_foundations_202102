package com.psybergate.grad2021.core.databasefnds.hw2b;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

public class DatabaseTable {

    public static void customerTable(Connection connection) throws SQLException {

        String sqlQuery = "create table if not exists customers (customerNum int primary key, name varchar, surname " +
                "varchar, startDate varchar);";

        Statement statement = connection.createStatement();
        statement.executeUpdate(sqlQuery);
        System.out.println("customers table created");
    }

    public static void accountsTable(Connection connection) throws SQLException {
        String sqlQuery = "create table if not exists accounts (accountNum int primary key, balance int, customerNum " +
                "int, constraint fk_01 foreign key (customerNum) references customers);";

        Statement statement = connection.createStatement();
        statement.executeUpdate(sqlQuery);
        System.out.println("accounts table created");
    }

    public static void transactionTable(Connection connection) throws SQLException {
        String sqlQuery = "create table if not exists transactions (transType varchar, accountNum int, accountType " +
                "varchar, date varchar, primary key (transType, accountNum, accountType, date))";

        Statement statement = connection.createStatement();
        statement.executeUpdate(sqlQuery);
        System.out.println("transactions table created");
    }
}
