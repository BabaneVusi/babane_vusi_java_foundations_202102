package com.psybergate.grad2021.core.exceptions.ce3;

public class ChainedExceptions {

    public static void main(String[] args) {
        try {
            anyMethod();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static void anyMethod() throws Exception {
        throwInitException();
    }

    private static void throwInitException() throws Exception {
        throw new Exception();
    }
}
