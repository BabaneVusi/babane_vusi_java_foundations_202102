package com.psybergate.grad2021.core.exceptions.ce1;

public class ExceptionSquashing {

    public static void main(String[] args) {
        try {
            anyMethod();
        }  catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static void anyMethod() throws Exception {
        throw new RuntimeException("Demonstrating exception squashing");
    }
}
