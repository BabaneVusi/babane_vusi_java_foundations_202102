package com.psybergate.grad2021.core.exceptions.hw4;

public class Controller {

    AccountService service;

    AccountValidator validate;

    public Controller() {
        this.service = new AccountService();
        this.validate = new AccountValidator();
    }

    public void addAccount(Account account) {
        try {
            validate.validateAccount(account.getUserIdentityNumber(), service);
            service.addAccount(account);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void deposit(Account account, double amount) {
        try {
            validate.search(account, service);
            service.add(account, amount);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void withdraw(Account account, double amount) {
        try {
            validate.search(account, service);
            validate.canWithdraw(account, amount);
            service.subtract(account, amount);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void printBalance(Account account) {
        try {
            validate.search(account, service);
            System.out.println("service.getBalance(account) = " + service.getBalance(account));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
