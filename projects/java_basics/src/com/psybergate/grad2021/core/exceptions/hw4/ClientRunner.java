package com.psybergate.grad2021.core.exceptions.hw4;

public class ClientRunner {

    public static void main(String[] args) {

        Account a1 = new SavingsAccount(1234, "Vusi");
        Account a2 = new SavingsAccount(12345, "Vuyo");
        Account a3 = new SavingsAccount(1234, "Vuyo");

        Controller control = new Controller();

        control.addAccount(a1);
        control.addAccount(a2);
//        control.addAccount(a3); // Account unique number already in the exist

        control.deposit(a1, 200);
        control.withdraw(a1, 50);
        control.printBalance(a1);
//        control.withdraw(a1,250); // Cannot over withdraw
    }
}
