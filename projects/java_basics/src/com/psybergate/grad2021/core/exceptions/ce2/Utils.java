package com.psybergate.grad2021.core.exceptions.ce2;

import java.util.Scanner;

import static java.lang.String.valueOf;

public class Utils {

    public static void main(String[] args) throws CheckedException, ThrowableException, ErrorExceptions {
        Scanner scan = new Scanner(System.in);
        System.out.println("Side of the square: ");
        int side = scan.nextInt();
        System.out.println("squareArea(side) = " + squareArea(side));
    }

    private static int squareArea(int side) throws CheckedException, ThrowableException, ErrorExceptions {
        if (side < 0) {
            throw new CheckedException(valueOf(side));
        } else if (side > 5000) {
            throw new RuntimeException(valueOf(side));
        } else if (side > 3000) {
            throw new ErrorExceptions();
        } else if (side > 1000) {
            throw new ThrowableException();
        }
        return (side * side);
    }
}
