package com.psybergate.grad2021.core.exceptions.hw2;

import java.sql.SQLException;

public class Utils {

    public static void main(String[] args) {
        ApplicationException appException = new ApplicationException();

        try {
            appException.printEntryUniqueNumber();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }
}
