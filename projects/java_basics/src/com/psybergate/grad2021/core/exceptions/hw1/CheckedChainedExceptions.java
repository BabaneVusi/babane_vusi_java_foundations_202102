package com.psybergate.grad2021.core.exceptions.hw1;

public class CheckedChainedExceptions {

    public static void main(String[] args) {
        try {
            receiveApplication();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static void receiveApplication() throws Exception {
        // application received.
        scanCurriculumVitae();
    }

    private static void scanCurriculumVitae() throws Exception {
        // CV meets requirements, second document to be scanned is ID
        scanIdentityDocument();
    }

    private static void scanIdentityDocument() throws Exception {
        // ID scanned, third document to be scanned are results
        scanTranscript();
    }

    private static void scanTranscript() throws Exception {
        applicationOutcome();
    }

    private static void applicationOutcome() throws Exception {
        throw new Exception("Application successful!");
    }
}
