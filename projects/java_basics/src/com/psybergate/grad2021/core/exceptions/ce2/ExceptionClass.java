package com.psybergate.grad2021.core.exceptions.ce2;

import java.util.Scanner;

public class ExceptionClass extends Throwable {

    public static void main(String[] args) throws Throwable {
        Scanner scan = new Scanner(System.in);
        System.out.println("Input 1, 2 or 3: ");
        int side = scan.nextInt();
        System.out.println("calculateSquareArea(side): " + calculateSquareArea(side));
    }

    public static int calculateSquareArea(int side) throws Throwable {
        if (side < 0) {
            runtimeException();
        } else if (side == 49) {
            checkException();
        } else if (side > 50) {
            throwableException();
        } else if ((side * side) > 2_400) {
            errorException();
        }
        return (side * side);
    }

    public static void errorException() {
        throw new Error("Area exceed maximum area of 4_500");
    }

    public static void throwableException() throws Throwable {
        throw new Throwable("Maximum side value of 50 is exceeded");
    }

    public static void checkException() throws Exception {
        throw new Exception("Area exceeds maximum by 1");
    }

    public static void runtimeException() {
        throw new RuntimeException("Side cannot be negative");
    }
}
