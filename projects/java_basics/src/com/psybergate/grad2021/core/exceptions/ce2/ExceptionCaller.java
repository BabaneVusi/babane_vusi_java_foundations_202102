package com.psybergate.grad2021.core.exceptions.ce2;

import java.util.Scanner;

public class ExceptionCaller {

    public static void main(String[] args) throws Throwable {

        Scanner scan = new Scanner(System.in);
        System.out.println("Input 1, 2 or 3: ");
        int code = scan.nextInt();
        if (code == 1) {
            callRuntimeException();
        } else if (code == 2) {
            callCheckException();
        } else if (code == 3) {
            callThrowableException();
        } else {
            callErrorException();
        }
    }

    public static void callErrorException() {
        ExceptionClass exception = new ExceptionClass();
        exception.errorException();
    }

    public static void callThrowableException() throws Throwable {
        ExceptionClass exception = new ExceptionClass();
        exception.throwableException();
    }

    public static void callCheckException() throws Exception {
        ExceptionClass exception = new ExceptionClass();
        exception.checkException();
    }

    public static void callRuntimeException() {
        ExceptionClass exception = new ExceptionClass();
        exception.runtimeException();
    }

}
