package com.psybergate.grad2021.core.exceptions.hw4;

import java.util.List;

public class AccountService {

    AccountDB accountDB;

    public AccountService() {
        this.accountDB = new AccountDB();
    }

    public void addAccount(Account account) {
        accountDB.list.add(account);
    }

    public List<Account> getAccounts() {
        return accountDB.list;
    }

    public void add(Account account, double amount) {
        account.deposit(amount);
    }

    public void subtract(Account account, double amount) {
        account.withdraw(amount);
    }

    public double getBalance(Account account) {
        return account.getBalance();
    }
}
