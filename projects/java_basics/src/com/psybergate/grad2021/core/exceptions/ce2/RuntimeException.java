package com.psybergate.grad2021.core.exceptions.ce2;

public class RuntimeException extends java.lang.RuntimeException {

    public RuntimeException(String message) {
        super(message + " exceeds the maximum side value (5000) limit");
    }
}
