package com.psybergate.grad2021.core.exceptions.hw1;

public class RuntimeCheckedExceptions {

    public static void main(String[] args) {
        try {
            receiveApplication();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static void receiveApplication() {
        // application received.
        scanCurriculumVitae();
    }

    private static void scanCurriculumVitae() {
        // CV meets requirements, second document to be scanned is ID
        scanIdentityDocument();
    }

    private static void scanIdentityDocument() {
        // ID scanned, third document to be scanned are results
        scanTranscript();
    }

    private static void scanTranscript() {
        applicationOutcome();
    }

    private static void applicationOutcome() {
        throw new RuntimeException("Application successful!");
    }
}
