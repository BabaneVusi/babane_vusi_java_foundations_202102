package com.psybergate.grad2021.core.exceptions.ce2;

public class CheckedException extends Exception {

    public CheckedException(String message) {
        super(message + " is an invalid side value");
    }
}
