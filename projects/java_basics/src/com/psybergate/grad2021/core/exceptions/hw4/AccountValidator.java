package com.psybergate.grad2021.core.exceptions.hw4;

public class AccountValidator {

    public void validateAccount(int uniqueID, AccountService service) throws Exception {
        for (Account account : service.getAccounts()) {
            if (uniqueID == account.getUserIdentityNumber()) {
                throw new Exception("Account already exists");
            }
        }
    }

    public Account search(Account account, AccountService service) throws Exception {
        for (Account acc : service.getAccounts()) {
            if (acc.getUserIdentityNumber() == account.getUserIdentityNumber()) {
                return acc;
            }
        }
        throw new Exception("Account does not exist");
    }


    public void canWithdraw(Account account, double amount) throws Exception {
        if (amount > account.getBalance()) {
            throw new Exception("Not enough funds to perform withdrawal");
        }
    }
}
