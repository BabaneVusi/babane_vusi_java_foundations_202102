package com.psybergate.grad2021.core.exceptions.hw4;

public class Account {

    private int userIdentityNumber;

    private String accountHolder;

    private double balance;

    public Account(int userIdentityNumber, String accountHolder) {
        this.userIdentityNumber = userIdentityNumber;
        this.accountHolder = accountHolder;
    }

    public int getUserIdentityNumber() {
        return userIdentityNumber;
    }

    @Override
    public String toString() {
        return "Account{" +
                "userIdentityNumber=" + userIdentityNumber +
                ", accountHolder='" + accountHolder + '\'' +
                '}';
    }

    public void deposit(double amount) {
        balance += amount;
    }

    public void withdraw(double amount){
        balance -= amount;
    }

    public double getBalance(){
        return balance;
    }
}
