package com.psybergate.grad2021.core.exceptions.ce2;

public class ErrorExceptions extends Throwable {
    public ErrorExceptions() {
        super("Side value exceeds the maximum limit 3_000");
    }
}
