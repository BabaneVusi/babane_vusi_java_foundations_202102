package com.psybergate.grad2021.core.exceptions.hw4;

import java.util.ArrayList;
import java.util.List;

public class AccountDB {

    List<Account> list;

    public AccountDB() {
        this.list = new ArrayList<>();
    }

    public List<Account> getList() {
        return list;
    }
}
