package com.psybergate.grad2021.core.exceptions.hw2;

import java.sql.SQLException;
import java.util.Random;

public class ApplicationException {

    private final static int MAXIMUM_LIMIT = 25;
    Random random = new Random();
    private int entryUniqueNumber;

    public ApplicationException() {
        entryUniqueNumber = random.nextInt(MAXIMUM_LIMIT);
    }

    public int getEntryUniqueNumber() throws SQLException {
        if (entryUniqueNumber >= 10) {
            throw new SQLException(entryUniqueNumber + " exceeds the maximum 20 available rows in the BD");
        }
        return entryUniqueNumber;
    }

    public void printEntryUniqueNumber() throws SQLException {
        System.out.println("getEntryUniqueNumber() = " + getEntryUniqueNumber());
    }
}
