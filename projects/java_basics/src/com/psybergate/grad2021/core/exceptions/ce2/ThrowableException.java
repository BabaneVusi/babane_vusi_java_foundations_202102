package com.psybergate.grad2021.core.exceptions.ce2;

public class ThrowableException extends Throwable {

    public ThrowableException() {
        super("Side value exceeds the maximum limit 1_000");
    }
}
