package com.psybergate.grad2021.core.generics.hw3a;

public class Accounts {

    private double balance;

    public Accounts(double balance) {
        this.balance = balance;
    }

    public void deposit(double amount){
        balance += amount;
    }

    public void withdraw(double amount){
        balance -= amount;
    }

    public double getBalance() {
        return balance;
    }
}
