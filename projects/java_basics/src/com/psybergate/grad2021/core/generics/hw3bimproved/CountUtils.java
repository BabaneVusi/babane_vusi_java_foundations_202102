package com.psybergate.grad2021.core.generics.hw3bimproved;

import java.util.Arrays;
import java.util.List;

public class CountUtils {

    public static void main(String[] args) {
        integers();
        strings();
    }

    private static void integers() {
        ElementCounter<Integer> count = new ElementCounter<>();
        List<Integer> list = Arrays.asList(1,2,3,4,5,6,7,8,9,10,15);

        System.out.println("count.countElements(list,new EvaluateEvenNumbers()) = " +
                count.countElements(list, new EvaluateEvenNumbers()));
        System.out.println("count.countElements(list,new EvaluateOddNumbers()) = " +
                count.countElements(list, new EvaluateOddNumbers()));
        System.out.println("count.countElements(list,new EvaluatePrimeNumbers()) = " +
                count.countElements(list, new EvaluatePrimeNumbers()));
    }

    private static void strings() {
        ElementCounter<String> count = new ElementCounter<>();
        List<String> list = Arrays.asList("avsdvcabd", "abwqe2wd", "abcsacd", "abdew", "abgfredxcvd", "abdde",
                "abdc", "abdcjdhj", "abdsd");

        System.out.println("count.countElements(list, new EvaluateStrings()) = " +
                count.countElements(list, new EvaluateStrings()));
    }
}
