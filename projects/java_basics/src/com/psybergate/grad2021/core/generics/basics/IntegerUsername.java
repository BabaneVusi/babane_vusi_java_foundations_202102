package com.psybergate.grad2021.core.generics.basics;

public class IntegerUsername {

    private Integer userName;

    public IntegerUsername(int userName) {
        this.userName = userName;
    }

    public int getUserName() {
        return userName;
    }
}

