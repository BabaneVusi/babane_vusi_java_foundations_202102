package com.psybergate.grad2021.core.generics.hw1b;

/**
 * Class file for the Team class after Erasure
 */


/**
 * Team class for Unbounded type
 */

public class Team {

    Object t;

    public Team() {
        this.t = null;
    }

    public void add(Object t) {
        this.t = t;
    }
}

/**
 * Team class for Bounded type
 */

// public class Team {
//
//    Person t;
//
//    public Team() {
//        this.t = null;
//    }
//
//    public void add(Person t) {
//        this.t = t;
//    }
// }


