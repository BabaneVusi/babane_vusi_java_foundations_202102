package com.psybergate.grad2021.core.generics.hw3b;

import java.util.Collection;

import static java.lang.Character.toLowerCase;

public class CountAnyElements<T> {

    public CountAnyElements() {
    }

    public int countElements(Collection<T> list, String characteristic) {
        int counter = 0;
        for (Object element : list) {
            if (isElement(element, characteristic)) {
                counter++;
            }
        }
        return counter;
    }

    private boolean isElement(Object element, String characteristic) {
        switch (characteristic) {
            case "Even":
                return isEven((Integer) element);
            case "Odd":
                return isOdd((Integer) element);
            case "Prime":
                return isPrime((Integer) element);
            case "String":
                return isStringContainC((String) element);
            default:
                throw new RuntimeException("invalid characteristic specification");
        }
    }

    private boolean isEven(Integer number) {
        return number % 2 == 0;
    }

    private boolean isOdd(Integer number) {
        return !isEven(number);
    }

    private boolean isPrime(Integer number) {
        int counter = 0;
        for (int i = number; i > 0; i--) {
            if (number % i == 0) {
                counter++;
            }
        }
        return (counter == 2);
    }

    private boolean isStringContainC(String string) {
        for (int i = 0; i < string.length(); i++) {
            if (toLowerCase(string.charAt(i)) == 'c') {
                return true;
            }
        }
        return false;
    }
}
