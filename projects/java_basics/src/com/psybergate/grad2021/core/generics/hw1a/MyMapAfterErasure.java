package com.psybergate.grad2021.core.generics.hw1a;

public class MyMapAfterErasure {

    /**
     * MyMap before erasure
     */
//     public class MyMap <K, V> {
//
//      private K key;
//      private V value;
//
//      public MyMap (K key, V value) {
//          this.key = key;
//          this.value = value;
//      }
//
//      public K getKey () {
//          return key;
//      }
//
//      public V getValue () {
//          return value;
//      }
//
//     public void setKey (K key) {
//          this.key = key;
//      }
//
//      public void setValue (V value) {
//          this.value = value;
//      }
//     }

    /**
     * After  erasure, the generic information is removed.
     * MyMap bytecode will be as shown below
     */

//     public class MyMap {
//
//      private Object key;
//      private Object value;
//
//      public MyMap (Object key, Object value) {
//          this.key = key;
//          this.value = value;
//      }
//
//      public Object getKey () {
//          return key;
//      }
//
//      public Object getValue () {
//          return value;
//      }
//
//     public void setKey (Object key) {
//          this.key = key;
//      }
//
//      public void setValue (Object value) {
//          this.value = value;
//      }
//     }
}
