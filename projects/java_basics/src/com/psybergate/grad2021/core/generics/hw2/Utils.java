package com.psybergate.grad2021.core.generics.hw2;

import java.util.ArrayList;
import java.util.List;

public class Utils {

    public static void main(String[] args) {
        List<Integer> list = integersCollection();

        NumberFilter numberFilter = new NumberFilter();
        printList((List<Integer>) numberFilter.greaterThan1000(list));
    }

    private static List<Integer> integersCollection() {
        List<Integer> list = new ArrayList<>();
        list.add(4567);
        list.add(345678);
        list.add(1000);
        list.add(34578);
        list.add(348);
        list.add(3456);
        return list;
    }

    private static void printList(List list) {
        for (int i = 0; i < list.size(); i++) {
            System.out.println("list.get(i) = " + list.get(i));
        }
    }
}
