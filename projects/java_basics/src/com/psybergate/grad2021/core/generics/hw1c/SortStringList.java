package com.psybergate.grad2021.core.generics.hw1c;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class SortStringList<T extends String> {

    public SortStringList() {
    }

    public static void main(String[] args) {
        SortStringList sortList = new SortStringList();
        List list = new ArrayList();
        list.add("abc");
        list.add("def");
        list.add("ghi");

        list = sortList.reverseList(list);
        printList(list);
    }

//    public static List reverseElements(List list) {
//        List newList = new ArrayList();
//
//        for (int i = 0; i < list.size(); i++) {
//            newList.add(reverseString((String) list.get(i)));
//        }
//        return newList;
//    }
//
//    public static String reverseString(String string) {
//        char[] chars = string.toCharArray();
//        String tempString = "";
//
//        for (int j = chars.length - 1; j >= 0; j--) {
//            tempString += chars[j];
//        }
//        return tempString;
//    }

    private static void printList(List list) {
        for (Iterator iter = list.iterator(); iter.hasNext(); ) {
            System.out.println("iter.next() = " + iter.next());
        }
    }

    public List<T> reverseList(List<T> list) {
        for (int i = 0; i < list.size(); i++) {
            list.set(i, reverseString(list.get(i)));
        }
        return list;
    }

    private T reverseString(T t) {
        StringBuilder tempString = new StringBuilder();
        for (int j = t.length() - 1; j >= 0; j--) {
            tempString.append(t.charAt(j));
        }
        return (T) tempString.toString();
//        T tempString = t;
//        for (int j = 0; j < t.length(); j++) {
//            tempString.charAt(j) = t.charAt(t.length() - 1); // Strings are immutable
//        }
//        return tempString;
    }
}
