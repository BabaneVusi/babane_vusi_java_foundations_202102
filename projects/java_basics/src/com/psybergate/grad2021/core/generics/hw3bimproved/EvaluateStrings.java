package com.psybergate.grad2021.core.generics.hw3bimproved;

import static java.lang.Character.toLowerCase;

public class EvaluateStrings extends Condition<String> {

    /**
     * Bridging method
     */
//    @Override
//    public boolean isSatisfiedBy(Object element) {
//        return isSatisfiedBy((String) element);
//    }

    public boolean isSatisfiedBy(String element) {
        for (int i = 0; i < element.length(); i++) {
            if (toLowerCase(element.charAt(i)) == 'c') {
                return true;
            }
        }
        return false;
    }
}
