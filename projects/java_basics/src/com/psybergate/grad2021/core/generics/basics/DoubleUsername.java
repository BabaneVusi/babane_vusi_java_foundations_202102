package com.psybergate.grad2021.core.generics.basics;

public class DoubleUsername {

    private Double userName;

    public DoubleUsername(double userName) {
        this.userName = userName;
    }

    public double getUserName() {
        return userName;
    }
}
