package com.psybergate.grad2021.core.generics.hw3a;

import java.util.ArrayList;
import java.util.List;

public class AccountUtils {

    public static void main(String[] args) {
        anyMethod();
    }

    private static void anyMethod() {
        AccountManager accountManager = new AccountManager();
        List<Accounts> list = new ArrayList<>();

        list.add(new Accounts(-20));
        list.add(new Accounts(120));
        list.add(new Accounts(-120));
        list.add(new Accounts(-1420));
        list.add(new Accounts(15420));
        list.add(new Accounts(-95120));
        list.add(new Accounts(-95120));
        list.add(new Accounts(-95120));
        list.add(new Accounts(405120));

        System.out.println("countOverdraftAccounts = " + accountManager.countOverdraftAccounts((ArrayList<Accounts>) list));
    }
}
