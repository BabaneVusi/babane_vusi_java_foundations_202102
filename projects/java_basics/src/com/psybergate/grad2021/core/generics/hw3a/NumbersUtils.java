package com.psybergate.grad2021.core.generics.hw3a;

import java.util.ArrayList;
import java.util.List;

public class NumbersUtils {

    public static void main(String[] args) {
        integers();
        strings();
    }

    private static void strings() {
        Numbers numbers = new Numbers();
        List<String> list = new ArrayList<>();
        list.add("avsdvcabd");
        list.add("abwqe2wd");
        list.add("abcsacd");
        list.add("abdew");
        list.add("abgfredxcvd");
        list.add("abdde");
        list.add("abdc");
        list.add("abdcjdhj");
        list.add("abdsd");

        System.out.println("nums.countStringWithC(list) = " + numbers.countStringWithC(list));
    }

    private static void integers() {
        Numbers numbers = new Numbers();
        List<Integer> list = new ArrayList<>();
        list.add(1);
        list.add(2);
        list.add(3);
        list.add(4);
        list.add(5);
        list.add(6);
        list.add(7);
        list.add(8);
        list.add(9);
        list.add(10);
        list.add(15);

        System.out.println("nums.countEvenNumbers(list) = " + numbers.countEvenNumbers(list));
        System.out.println("nums.countOddNumbers(list) = " + numbers.countOddNumbers(list));
        System.out.println("nums.countPrimeNumbers(list) = " + numbers.countPrimeNumbers(list));
    }
}
