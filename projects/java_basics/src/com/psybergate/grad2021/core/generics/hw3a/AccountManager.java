package com.psybergate.grad2021.core.generics.hw3a;

import java.util.ArrayList;

public class AccountManager {

    public int countOverdraftAccounts(ArrayList<Accounts> list) {
        int counter = 0;
        for (Accounts account : list) {
            if (isAccountOverdraft(account)) {
                counter++;
            }
        }
        return counter;
    }

    private boolean isAccountOverdraft(Accounts account) {
        return account.getBalance() < 0;
    }
}
