package com.psybergate.grad2021.core.generics.hw3bimproved;

import java.util.Collection;

public class ElementCounter<T> {

    public ElementCounter() {
    }

    public int countElements(Collection elements, Condition condition) {
        int counter = 0;
        for (Object element : elements) {
            if (condition.isSatisfiedBy(element)) {
                counter++;
            }
        }
        return counter;
    }
}
