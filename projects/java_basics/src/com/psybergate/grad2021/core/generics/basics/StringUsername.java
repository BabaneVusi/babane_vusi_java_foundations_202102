package com.psybergate.grad2021.core.generics.basics;

public class StringUsername {

    private String userName;

    public StringUsername(String userName) {
        this.userName = userName;
    }

    public String getUserName() {
        return userName;
    }
}
