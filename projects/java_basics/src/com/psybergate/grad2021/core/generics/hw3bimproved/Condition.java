package com.psybergate.grad2021.core.generics.hw3bimproved;

public abstract class Condition<T> {

    public abstract boolean isSatisfiedBy(T element);
}

//public abstract class Condition {
//
//    public abstract boolean isSatisfiedBy(Object element);
//}
