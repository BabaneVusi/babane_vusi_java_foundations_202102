package com.psybergate.grad2021.core.generics.hw3b;

import java.util.Arrays;
import java.util.List;

public class CountUtils {

    public static void main(String[] args) {
        integers();
        strings();
    }

    private static void integers() {
        CountAnyElements<Integer> count = new CountAnyElements<>();
        List<Integer> list = Arrays.asList(1,2,3,4,5,6,7,8,9,10,15);

        System.out.println("count.countElements(list, \"Even\") = " + count.countElements(list, "Even"));
        System.out.println("count.countElements(list, \"Even\") = " + count.countElements(list, "Odd"));
        System.out.println("count.countElements(list, \"Even\") = " + count.countElements(list, "Prime"));
    }

    private static void strings() {
        CountAnyElements<String> count = new CountAnyElements<>();
        List<String> list = Arrays.asList("avsdvcabd", "abwqe2wd", "abcsacd", "abdew", "abgfredxcvd", "abdde",
                "abdc", "abdcjdhj", "abdsd");

        System.out.println("count.countElements(list, \"String\") = " + count.countElements(list, "String"));
    }
}
