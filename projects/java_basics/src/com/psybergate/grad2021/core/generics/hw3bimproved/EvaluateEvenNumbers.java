package com.psybergate.grad2021.core.generics.hw3bimproved;

import java.lang.reflect.Method;

public class EvaluateEvenNumbers extends Condition<Integer> {

    /**
     * Bridging method
     */
//    @Override
//    public boolean isSatisfiedBy(Object element) {
//        return isSatisfiedBy((Integer) element);
//    }

    public static void main(String[] args) {
        Method[] methods = EvaluateEvenNumbers.class.getMethods();
        System.out.println("methods.length = " + methods.length);
        for (Method method : methods) {
            System.out.println("method.getName() = " + method.getName() + " is bridge = " + method.isBridge());
        }
    }

    @Override
    public boolean isSatisfiedBy(Integer element) {
//        String a = null;
//        a.concat("ajsn");
        return element % 2 == 0;
    }
}
