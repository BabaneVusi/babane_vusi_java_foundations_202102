package com.psybergate.grad2021.core.generics.basics;

public class ExploringGenerics {
    public static void main(String[] args) {
//        nonGenericClasses();
        genericClasses();
        Object obj = new Object();
    }

    private static void genericClasses() {
        UserName<Integer> number1 = new UserName(6);
//        UserName<String> number11 = new UserName(6); // Compiles
//        UserName<int> number1 = new UserName(6); // Cannot pass a primitive to the generic type
        UserName<Double> number2 = new UserName(7.3);
//        UserName<Double> number2 = new UserName("Vuyo"); // This compiled and run... How? - String is cast to Double?
        UserName<String> number3 = new UserName("Vusi");

        System.out.println("number1.getUserName() = " + number1.getUserName());
        System.out.println("number2.getUserName() = " + number2.getUserName());
        System.out.println("number3.getUserName() = " + number3.getUserName());
    }

    private static void nonGenericClasses() {
        IntegerUsername number1 = new IntegerUsername(6);
        DoubleUsername number2 = new DoubleUsername(7.3);
        StringUsername number3 = new StringUsername("Vusi");

        System.out.println("number1.getUserName() = " + number1.getUserName());
        System.out.println("number2.getUserName() = " + number2.getUserName());
        System.out.println("number3.getUserName() = " + number3.getUserName());
    }
}
