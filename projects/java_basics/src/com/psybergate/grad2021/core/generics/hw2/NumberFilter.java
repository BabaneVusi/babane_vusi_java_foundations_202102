package com.psybergate.grad2021.core.generics.hw2;

import java.util.Collection;

public class NumberFilter {

    public NumberFilter() {
    }

    public Collection<Integer> greaterThan1000(Collection<Integer> list) {
        list.removeIf(integer -> integer <= 1000);
        return list;
    }
}
