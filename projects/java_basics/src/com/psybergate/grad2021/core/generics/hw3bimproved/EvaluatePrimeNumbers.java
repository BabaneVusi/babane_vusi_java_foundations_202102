package com.psybergate.grad2021.core.generics.hw3bimproved;

public class EvaluatePrimeNumbers extends Condition<Integer> {

    @Override
    public boolean isSatisfiedBy(Integer element) {
        int counter = 0;
        for (int i = element; i > 0; i--) {
            if (element % i == 0) {
                counter++;
            }
        }
        return (counter == 2);

    }
}
