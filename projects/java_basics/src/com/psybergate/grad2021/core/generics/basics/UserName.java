package com.psybergate.grad2021.core.generics.basics;

public class UserName<T> {

    private T userName;

    public UserName(T userName) {
        this.userName = userName;
    }

    public T getUserName() {
        return userName;
    }
}
