package com.psybergate.grad2021.core.generics.hw3a;

import java.util.Collection;

import static java.lang.Character.toLowerCase;

public class Numbers {

    public int countEvenNumbers(Collection<Integer> coll) {
        int counter = 0;
        for (Integer integer : coll) {
            if (isEven(integer)) {
                counter++;
            }
        }
        return counter;
    }

    private boolean isEven(int number) {
        return number % 2 == 0;
    }

    public int countOddNumbers(Collection<Integer> list) {
        int counter = 0;
        for (Integer element : list) {
            if (isOdd(element)) {
                counter++;
            }
        }
        return counter;
    }

    private boolean isOdd(int i) {
        return !isEven(i);
    }

    public int countPrimeNumbers(Collection<Integer> list) {
        int counter = 0;
        for (Integer element : list) {
            if (isPrime(element)) {
                counter++;
            }
        }
        return counter;
    }

    private boolean isPrime(int number) {
        int counter = 0;
        for (int i = number; i > 0; i--) {
            if (number % i == 0) {
                counter++;
            }
        }
        return (counter == 2);
    }

    public int countStringWithC(Collection<String> list) {
        int counter = 0;
        for (String string : list) {
            if (isStringContainC(string)) {
                counter++;
            }
        }
        return counter;
    }

    private boolean isStringContainC(String string) {
        for (int i = 0; i < string.length(); i++) {
            if (toLowerCase(string.charAt(i)) == 'c') {
                return true;
            }
        }
        return false;
    }
}
