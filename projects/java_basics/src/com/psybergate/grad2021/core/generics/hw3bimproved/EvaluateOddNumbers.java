package com.psybergate.grad2021.core.generics.hw3bimproved;

public class EvaluateOddNumbers extends Condition<Integer> {

    @Override
    public boolean isSatisfiedBy(Integer element) {
        return element % 2 != 0;
    }
}
