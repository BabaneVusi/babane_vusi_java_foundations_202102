package com.psybergate.grad2021.core.interfaces.samemethods;

public interface InterfaceB {

  default void anyMethod() {
    System.out.println("Interface B");
  }
}
