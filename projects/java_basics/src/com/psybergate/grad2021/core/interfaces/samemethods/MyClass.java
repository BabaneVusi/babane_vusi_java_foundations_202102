package com.psybergate.grad2021.core.interfaces.samemethods;

public class MyClass implements InterfaceA, InterfaceB{
  @Override
  public void anyMethod() {
    System.out.println("Myclass");
  }
}
