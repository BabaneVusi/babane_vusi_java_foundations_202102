package com.psybergate.grad2021.core.interfaces.samemethods;

public interface InterfaceA {

  default void anyMethod(){
    System.out.println("Interface A");
  }
}
