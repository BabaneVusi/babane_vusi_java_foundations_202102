package com.psybergate.grad2021.core.interfaces.samemethods;

public class Utils {

  public static void main(String[] args) {
    InterfaceA interfaceA = new MyClass();
    InterfaceB interfaceB = new MyClass();
    interfaceA.anyMethod();
    interfaceB.anyMethod();
  }
}
