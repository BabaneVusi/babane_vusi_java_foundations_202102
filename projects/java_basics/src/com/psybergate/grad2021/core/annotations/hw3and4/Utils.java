package com.psybergate.grad2021.core.annotations.hw3and4;

import java.sql.Connection;
import java.sql.SQLException;

import static com.psybergate.grad2021.core.annotations.hw3and4.CustomerService.getCustomer;
import static com.psybergate.grad2021.core.annotations.hw3and4.DatabaseConnection.createConnection;

public class Utils {

    public static void main(String args[]) throws NoSuchFieldException, SQLException {
        Connection connection = createConnection();
//        generateDatabase(connection);
        Customer c1 = new Customer("12356", "jeremy", "notyesi", 19990419, 22);
//        saveCustomer(c1, connection);
        String s1 = "12356";
        String s2 = "1234456";
        String s3 = "12332";
        Customer c2 = getCustomer(s3, connection);
        System.out.println("c2.getName() = " + c2.getName());
    }
}
