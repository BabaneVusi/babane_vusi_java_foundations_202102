package com.psybergate.grad2021.core.annotations.hw2;

@DomainClass
public class Customer {

    @DomainProperty
    private String customerNum;

    @DomainProperty
    private String name;

    @DomainProperty
    private String surname;

    @DomainProperty
    private Integer dateOfBirth;

    @DomainTransient
    private int age;

    public Customer(String customerNum, String name, String surname, Integer dateOfBirth, int age) {
        this.customerNum = customerNum;
        this.name = name;
        this.surname = surname;
        this.dateOfBirth = dateOfBirth;
        this.age = age;
    }
}
