package com.psybergate.grad2021.core.annotations.hw2;

import java.lang.annotation.*;

@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD, ElementType.PARAMETER})

public @interface DomainTransient {
}
