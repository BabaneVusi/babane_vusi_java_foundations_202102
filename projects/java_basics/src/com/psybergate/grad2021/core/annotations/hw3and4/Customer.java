package com.psybergate.grad2021.core.annotations.hw3and4;

@DomainClass
public class Customer {

    @DomainProperty (name = "customerNum")
    private String customerNum;

    @DomainProperty (name = "name")
    private String name;

    @DomainProperty (name = "surName")
    private String surname;

    @DomainProperty (name = "dateOfBirth")
    private Integer dateOfBirth;

    @DomainTransient
    private int age;

    public Customer(String customerNum, String name, String surname, Integer dateOfBirth, int age) {
        this.customerNum = customerNum;
        this.name = name;
        this.surname = surname;
        this.dateOfBirth = dateOfBirth;
        this.age = age;
    }

    public Customer(String customerNum, String name, String surname, Integer dateOfBirth) {
        this(customerNum, name, surname, dateOfBirth, 0);
    }

    public String getCustomerNum() {
        return customerNum;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public Integer getDateOfBirth() {
        return dateOfBirth;
    }
}
