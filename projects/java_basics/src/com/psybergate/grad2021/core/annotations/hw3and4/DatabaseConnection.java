package com.psybergate.grad2021.core.annotations.hw3and4;

import java.sql.Connection;
import java.sql.DriverManager;

public class DatabaseConnection {

    public static Connection createConnection() {
        Connection connection = null;
        try {
            Class.forName("org.postgresql.Driver");
            connection = DriverManager
                    .getConnection("jdbc:postgresql://localhost:5432/customer_storage",
                            "postgres", "admin");
            System.out.println("Opened database successfully");
        } catch (Exception e) {
            e.printStackTrace();
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
            System.exit(0);
        }
        return connection;
    }
}
