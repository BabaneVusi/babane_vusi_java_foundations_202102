package com.psybergate.grad2021.core.annotations.hw3and4;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

public class DatabaseManager {

    public static void generateDatabase(Connection conn) throws NoSuchFieldException, SQLException {
        Class customer = Customer.class;
        Field[] fields = customer.getDeclaredFields();

        StringBuilder sqlStatement = new StringBuilder();
        sqlStatement.append("Create Table customer (");
        for (Field field : fields) {
            Annotation[] annotations = field.getAnnotations();
            for (Annotation annotation : annotations) {
                if (annotation instanceof DomainProperty) {
                    sqlStatement.append(field.getName());
                    if (field.getType().getSimpleName().equals("String")) {
                        sqlStatement.append(" varchar, ");
                    } else {
                        sqlStatement.append(" int ");
                    }
                    break;
                }
            }
        }
        sqlStatement.append(")");
        System.out.println("sqlStatement.toString() = " + sqlStatement.toString());
        Statement statement = conn.createStatement();
        statement.executeUpdate(String.valueOf(sqlStatement));
        System.out.println("Table created!");
    }
}
