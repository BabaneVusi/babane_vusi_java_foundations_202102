package com.psybergate.grad2021.core.annotations.hw3and4;

import java.lang.annotation.*;

@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD, ElementType.PARAMETER})

public @interface DomainTransient {

    boolean PrimaryKey() default false;

    boolean nullable() default false;
}
