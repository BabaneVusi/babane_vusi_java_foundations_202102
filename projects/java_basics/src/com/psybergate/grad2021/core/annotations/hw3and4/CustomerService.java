package com.psybergate.grad2021.core.annotations.hw3and4;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.sql.*;
import java.util.*;

public class CustomerService {

    public static void saveCustomer(Customer customer, Connection connection) throws SQLException {
        StringBuilder sqlStatement = new StringBuilder();
        sqlStatement.append("insert into customer values (");

        List<Field> fields = Arrays.asList(customer.getClass().getDeclaredFields());

        for (Iterator iterator = fields.iterator(); iterator.hasNext(); ) {
            Field field = (Field) iterator.next();
            Annotation[] annotations = field.getAnnotations();
            for (Annotation annotation : annotations) {
                if (annotation instanceof DomainProperty) {
                    sqlStatement.append(getValue(customer, field.getName()));
                }
            }

            if (iterator.hasNext()) {
                sqlStatement.append(",");
            }
        }
        String sql = sqlStatement.substring(0, sqlStatement.length() - 1);
        sql = sql + ")";
        System.out.println("sqlStatement = " + sql);
        Statement statement = connection.createStatement();
        statement.executeUpdate(String.valueOf(sql));
        System.out.println("customer saved");
    }

    private static String getValue(Customer customer, String field) {
        String value = "";
        switch (field) {
            case "customerNum":
                value = "'" + customer.getCustomerNum() + "'";
                break;
            case "name":
                value = "'" + customer.getName() + "'";
                break;
            case "surname":
                value = "'" + customer.getSurname() + "'";
                break;
            case "dateOfBirth":
                value = String.valueOf(customer.getDateOfBirth());
                break;
        }
        return value;
    }

    public static Customer getCustomer(String customerNum, Connection connection) throws SQLException {

        List<Object> data = new ArrayList();
        connection.setAutoCommit(false);
        Statement executor = connection.createStatement();
        ResultSet output = executor.executeQuery("select * from customer;");

        addData(customerNum, data, output);
        return createCustomerObject(data);
    }

    private static void addData(String customerNum, List<Object> data, ResultSet rows) throws SQLException {
        while (rows.next()) {
            if (customerNum.equals(rows.getString("customerNum"))) {
                for (String fieldName : getFields().keySet()) {
                    String fieldType = getFields().get(fieldName);
                    switch (fieldType) {
                        case "String":
                            data.add(rows.getString(fieldName));
                            break;
                        case "Integer":
                            data.add(rows.getInt(fieldName));
                            break;
                    }
                }
            }
        }
    }

    public static Customer createCustomerObject(List<Object> data) {
        Customer customer =
                new Customer((String) data.get(0), (String) data.get(1), (String) data.get(2), (Integer) data.get(3));
        return customer;
    }

    private static Map<String, String> getFields() {
        Map<String, String> databaseRows = new LinkedHashMap<>();

        Field[] customerFields = Customer.class.getDeclaredFields();

        for (Field field : customerFields) {
            Annotation[] annotations = field.getAnnotations();
            for (Annotation annotation : annotations) {
                if (annotation instanceof DomainProperty) {
                    String fieldName = ((DomainProperty) annotation).name();
                    databaseRows.put(fieldName, field.getType().getSimpleName());
                    break;
                }
            }
        }
        return databaseRows;
    }
}
