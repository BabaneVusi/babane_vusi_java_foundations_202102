package com.psybergate.grad2021.core.lamdus.demo;

@FunctionalInterface
interface Work {
  public abstract void print(Integer element);
}
