package com.psybergate.grad2021.core.lamdus.ce1a;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class StreamInterface {

  private static List<String> people = Arrays.asList("Abe", "A", "Vv", "Vusi", "Vuyo", "sminky", "Babane", "Aviwe", "Sphenathi");

  public static void main(String[] args) {
//    getStream();
//    filtering();
//    mapping();
    reducing();
  }

  private static void reducing() {
//    Optional optional = people.stream().reduce((name1, name2) -> name1.length() > name2.length() ? name1 : name2);
    Optional optional = people.stream().reduce((name1, name2) -> name1 + name2.length());
    System.out.println("optional = " + optional);
  }

  private static void getStream() {
    System.out.println("people = " + people);
//    System.out.println("people.stream() = " + people.stream()); // Does not print the names in the list
    // I guess it a stream - how does a stream look?
//    List<String> list = (List<String>) people.stream(); // Does not compile. Not a list but returns a stream.
//    List<String> list = (List<String>) people.stream(); // Cannot cast as well.
    List<String> list1 = people.stream().collect(Collectors.toList());
    System.out.println("list1 = " + list1);
    people.stream().forEach(System.out::println);
  }

  public static void filtering() {
    Predicate<String> isLength = num -> num.length() > 4; // trying to write a predicate
//    people.stream().filter(name -> name.length() > 4).forEach(System.out::println);
    people.stream().filter(isLength).forEach(System.out::println);
  }

  public static void mapping() {
    people.stream().map(String::toUpperCase).forEach(System.out::println);
    people.stream().map(String::length).forEach(System.out::println);
  }
}
