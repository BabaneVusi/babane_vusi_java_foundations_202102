package com.psybergate.grad2021.core.lamdus.demo;

import java.util.Arrays;
import java.util.List;

public class Printer {

  private List<Integer> list = Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10);

  public Printer() {
  }

  public void forEachElement(Work work) {
    for (Integer element : list) {
      work.print(element);
    }
  }

  //  public void squares() {
//    forEachElement(new Work() {
//      @Override
//      public void print(Integer element) {
//        System.out.println("square: " + (element * element));
//      }
//    });
//  }

  public void squares() {
    forEachElement(element -> System.out.println("square = " + (element * element)));
  }

  public void mathod() {
//    Work work = name -> System.out.println(name); // Because we are just printing out the input, there is a short hand
    // version of this lambda
//    Work work = name -> System.out::println; // does not compile - I think this is wrong syntax
    Work work = System.out::println;

    int i = 10;
    work.print(i);
  }
}
