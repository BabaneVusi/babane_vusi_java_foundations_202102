package com.psybergate.grad2021.core.collections.basiccollections.hw2;

public class Employee {

    private int employeeNum;

    private int annualSalary;

    public Employee(int employeeNum, int annualSalary) {
        this.employeeNum = employeeNum;
        this.annualSalary = annualSalary;
    }

    public int getEmployeeNum() {
        return employeeNum;
    }

    public int getAnnualSalary() {
        return annualSalary;
    }

    @Override
    public String toString() {
        return "Employee{" +
                "employeeNum=" + employeeNum +
                ", positionName='" + annualSalary + '\'' +
                '}';
    }
}
