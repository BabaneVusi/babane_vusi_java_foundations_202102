package com.psybergate.grad2021.core.collections.basiccollections.list;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;


public class ListDemoUtils {

    public static void main(String[] args) {
//        arrayList();
//        linkedList();
        testCustomClass();
    }

    private static void testCustomClass() {
        List list = new CustomList();
        list.add(10);
        list.add(450) ;
        list.add(140);
        list.add(550);
        printCollection(list);
    }

    private static void linkedList() {
        List list = new LinkedList();
        list.add(1);
        list.add(45);
        list.add(14);
        list.add(55);
        printCollection(list);
    }

    private static void arrayList() {
        List list = new ArrayList();
        list.add(new Integer(1)); // boxing - creating an object
        list.add(2);
        list.add(5);
        list.add(3);
        printCollection(list);
    }

    private static void printCollection(List list) {
        for (Iterator iterator = list.iterator(); iterator.hasNext();) {
            System.out.println("iterator.next() = " + iterator.next());
        }
    }

}
