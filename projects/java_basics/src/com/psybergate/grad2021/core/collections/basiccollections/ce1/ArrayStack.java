package com.psybergate.grad2021.core.collections.basiccollections.ce1;

import java.util.Collection;
import java.util.Iterator;

public class ArrayStack implements Collection {

    private static final int ZERO = 0;

    private static final int TWO = 2;

    private Object[] list = new Object[100];

    private int listCapacity = list.length;

    private int listSize;

    public ArrayStack() {
        this.listSize = ZERO;
    }

    @Override
    public int size() {
        return listSize;
    }

    @Override
    public boolean isEmpty() {
        return size() == ZERO;
    }

    @Override
    public boolean contains(Object o) {
        for (Iterator iterator = iterator(); iterator.hasNext(); ) {
            if (iterator.next().equals(o)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public Iterator iterator() {
        return new myIterator(list, size());
    }

    @Override
    public Object[] toArray() {
        return new Object[0];
    }

    public boolean push(Object o) {
        if (listSize == listCapacity) {
            increaseListSize();
        }

        list[listSize++] = o;
        return true;
    }

    private void increaseListSize() {
        Object[] tempList = new Object[listCapacity * TWO];
        for (int i = 0; i < size() - 1; i++) {
            tempList[i] = list[i];
        }
        list = tempList;
    }

    public Object pop() {
        if ((listSize - 1) < ZERO) {
            throw new ArrayIndexOutOfBoundsException();
        }

        Object tempObject = list[listSize - 1];
        list[--listSize] = null;
        return tempObject;
    }

    public Object get(int position) {
        if (position < ZERO || position > listSize) {
            throw new ArrayIndexOutOfBoundsException();
        }

        return list[position];
    }

    @Override
    public boolean add(Object o) {
        return push(o);
    }

    @Override
    public boolean remove(Object o) {
        return false;
    }

    @Override
    public boolean addAll(Collection c) {
        return false;
    }

    @Override
    public void clear() {

    }

    @Override
    public boolean retainAll(Collection c) {
        return false;
    }

    @Override
    public boolean removeAll(Collection c) {
        return false;
    }

    @Override
    public boolean containsAll(Collection c) {
        return false;
    }

    @Override
    public Object[] toArray(Object[] a) {
        return new Object[0];
    }
}
