package com.psybergate.grad2021.core.collections.basiccollections.hw3;

import java.util.Comparator;

public class ReserveOrder implements Comparator<String> {

    public ReserveOrder() {
    }

    @Override
    public int compare(String o1, String o2) {
        return (o2).compareTo(o1);
    }
}
