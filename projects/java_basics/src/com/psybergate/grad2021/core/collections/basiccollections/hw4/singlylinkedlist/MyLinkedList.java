package com.psybergate.grad2021.core.collections.basiccollections.hw4.singlylinkedlist;

import java.util.Iterator;

public class MyLinkedList extends MyAbstractLinkedList {

    public Node head;

    public Node currentNode;

    private int size = 0;

    public MyLinkedList() {
        this.head = null;
        this.currentNode = null;
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        return head == null;
    }

    @Override
    public boolean contains(Object o) {
        for (Iterator iterator = iterator(); iterator.hasNext(); ) {
            if (iterator.next().equals(o)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public Iterator iterator() {
        return new MyIterator(this);
    }

    @Override
    public boolean add(Object o) {
        if (isEmpty()) {
            head = new Node(o);
            currentNode = head;
            size++;
            return true;
        }

        if (currentNode.next != null) {
            currentNode = currentNode.next;
        }

        currentNode.next = new Node(o);
        size++;
        return true;
    }

    @Override
    public boolean remove(Object o) {
        return false;
    }
}
