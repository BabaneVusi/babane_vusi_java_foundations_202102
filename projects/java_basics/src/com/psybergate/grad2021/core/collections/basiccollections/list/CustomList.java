package com.psybergate.grad2021.core.collections.basiccollections.list;


import java.util.Collection;
import java.util.Iterator;
import java.util.List;

public class CustomList implements List {

    private Object[] myList = new Object[100];

    private int listSize;

    private int listCapacity = myList.length;

    public CustomList() {
        listSize = 0;
    }

    @Override
    public int size() {
        return listSize;
    }

    @Override
    public boolean isEmpty() {
        return size() == 0;
    }

    @Override
    public boolean contains(Object o) {
        return false;
    }

    @Override
    public Iterator iterator() {
        return new ListIterator(myList, size());
    }

    @Override
    public Object[] toArray() {
        return new Object[0];
    }

    @Override
    public boolean add(Object o) {
        if (listSize == listCapacity) {
            doubleList();
        }
        myList[listSize++] = o;
        return true;
    }

    private void doubleList() {
        listCapacity = listCapacity * 2;
        Object[] tempList = new Object[listCapacity];
        for (int i = 0; i < size(); i++) {
            tempList[i] = myList[i];
        }
        myList = tempList;
    }

    @Override
    public boolean remove(Object o) {
        for (int i = 0; i < size(); i++) {
            if (myList[i] == o) {
                shiftElementsBackward(i);
                listSize--;
                return true;
            }
        }
        return false;
    }

    private void shiftElementsBackward(int i) {
        for (int a = i; a < size() - 1; a++) {
            myList[a] = myList[a + 1];
        }
        myList[size() - 1] = null;
    }

    @Override
    public boolean addAll(Collection c) {
        return false;
    }

    @Override
    public boolean addAll(int index, Collection c) {
        return false;
    }

    @Override
    public void clear() {

    }

    @Override
    public Object get(int index) {
        if (index < 0 || index > listSize) {
            throw new ArrayIndexOutOfBoundsException();
        }
        return myList[index];
    }

    @Override
    public Object set(int index, Object element) {
        if (index < 0 || index > listSize) {
            throw new ArrayIndexOutOfBoundsException();
        }

        Object previousElement = myList[index];
        myList[index] = element;
        return previousElement;
    }

    @Override
    public void add(int index, Object element) {

    }

    @Override
    public Object remove(int index) {
        return null;
    }

    @Override
    public int indexOf(Object o) {
        return 0;
    }

    @Override
    public int lastIndexOf(Object o) {
        return 0;
    }

    @Override
    public java.util.ListIterator listIterator() {
        return null;
    }

    @Override
    public java.util.ListIterator listIterator(int index) {
        return null;
    }

    @Override
    public List subList(int fromIndex, int toIndex) {
        return null;
    }

    @Override
    public boolean retainAll(Collection c) {
        return false;
    }

    @Override
    public boolean removeAll(Collection c) {
        return true;
    }

    @Override
    public boolean containsAll(Collection c) {
        return false;
    }

    @Override
    public Object[] toArray(Object[] a) {
        return new Object[0];
    }
}
