package com.psybergate.grad2021.core.collections.basiccollections.hw2;

import java.util.*;

public class EmployeeUtils {

    public static void main(String[] args) {
//        SortedMap map = new TreeMap<>(new AscendingOrder());
        SortedMap map = new TreeMap<>(new DescendingOrder());

        Employee e1 = new Employee(1,1000);
        Employee e2 = new Employee(2,2000);
        Employee e5 = new Employee(5,5000);
        Employee e3 = new Employee(3,3000);
        Employee e4 = new Employee(4,4000);

        map.put(e1.getEmployeeNum(), e1.getAnnualSalary());
        map.put(e2.getEmployeeNum(), e2.getAnnualSalary());
        map.put(e3.getEmployeeNum(), e3.getAnnualSalary());
        map.put(e4.getEmployeeNum(), e4.getAnnualSalary());
        map.put(e5.getEmployeeNum(), e5.getAnnualSalary());

        printEmployees(map);
    }

    private static void printEmployees(SortedMap map) {
        Set keys = map.keySet();
        int i = 0;
        for (final Object key : keys) {
            System.out.println("map:" + i++ + "(" + key + ")  = " + map.get(key));
        }
    }
}
