package com.psybergate.grad2021.core.collections.basiccollections.hw3;

import java.util.Iterator;
import java.util.SortedSet;
import java.util.TreeSet;

public class StringsCollection {

    public static void main(String[] args) {
        SortedSet set = new TreeSet<>(new ReserveOrder());

        set.add("ae");
        set.add("bd");
        set.add("cc");
        set.add("db");
        set.add("ea");
        printSet(set);
    }

    private static void printSet(SortedSet set) {
        for (Iterator iter = set.iterator(); iter.hasNext();) {
            System.out.println("iter.next() = " + iter.next());
        }
    }
}
