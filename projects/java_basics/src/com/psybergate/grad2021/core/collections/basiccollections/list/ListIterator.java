package com.psybergate.grad2021.core.collections.basiccollections.list;

import java.util.Iterator;

public class ListIterator implements Iterator {

    private Object[] list;

    private int index;

    private int size;

    public ListIterator(Object[] list, int size) {
        this.list = list;
        this.size = size;
        index = 0;
    }

    @Override
    public boolean hasNext() {
        return index != size;
    }

    @Override
    public Object next() {
        if (!hasNext()) {
            throw new ArrayIndexOutOfBoundsException();
        }

        return list[index++];
    }
}
