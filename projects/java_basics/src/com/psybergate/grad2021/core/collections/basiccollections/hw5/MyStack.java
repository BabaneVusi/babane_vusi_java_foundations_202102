package com.psybergate.grad2021.core.collections.basiccollections.hw5;

import java.util.Collection;

public interface MyStack extends Collection {

    public void push(Object o); // adds an object to the top of the stack;

    public Object pop(); // returns the top object and removes it from the stack

    public Object get(int position); // returns the Object at
}
