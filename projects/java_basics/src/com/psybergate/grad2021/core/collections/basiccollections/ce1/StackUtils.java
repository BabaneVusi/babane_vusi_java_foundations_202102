package com.psybergate.grad2021.core.collections.basiccollections.ce1;

import java.util.Iterator;

public class StackUtils {

    public static void main(String[] args) {
        ArrayStack stack = new ArrayStack();
        stack.push(1);
        stack.push(39);
        stack.push(90);
        stack.push(-2);
        System.out.println("stack.pop() = " + stack.pop());
        stack.push(-2);
        stack.push(-5);
        stack.push(-5777);
        stack.push(-5);
        System.out.println("stack.pop() = " + stack.pop());
        stack.push(40);
        printStack(stack);
        System.out.println("stack.contains(-5) = " + stack.contains(-5));
        System.out.println("stack.contains(1000) = " + stack.contains(1000));
        System.out.println("stack.size() = " + stack.size());
    }

    private static void printStack(ArrayStack stack) {
        for (Iterator iter = stack.iterator(); iter.hasNext();) {
            System.out.println("iter.next() = " + iter.next());
        }
    }
}
