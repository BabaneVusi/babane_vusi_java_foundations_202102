package com.psybergate.grad2021.core.collections.basiccollections.hw4.singlylinkedlist;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class MyIterator implements Iterator {

    private MyLinkedList list;

    private Node head;

    public MyIterator(MyLinkedList list) {
        this.list = list;
        this.head = list.head;
    }

    @Override
    public boolean hasNext() {
        if (head == null){
            return false;
        }

        return true;
    }

    @Override
    public Object next() {
        if (!hasNext()) {
            throw new NoSuchElementException("No such element");
        }

        Node tempNode = head;
        head = head.next;
        return tempNode.data;
    }
}
