package com.psybergate.grad2021.core.collections.basiccollections.hw4.singlylinkedlist;

public class Node {

    public Object data;

    public Node next;

    public Node(Object data) {
        this.data = data;
        this.next = null;
    }

}
