package com.psybergate.grad2021.core.collections.basiccollections.hw5;

import java.util.Iterator;

public class MyIterator implements Iterator {

    private Object[] list;

    private int listSize;

    private int index;

    public MyIterator(Object[] list, int listSize) {
        this.list = list;
        this.listSize = listSize - 1;
        this.index = 0;
    }

    @Override
    public boolean hasNext() {
        return (listSize >= index);
    }

    @Override
    public Object next() {
        if (!hasNext()) {
            throw new ArrayIndexOutOfBoundsException();
        }
        return list[listSize - index++];
    }
}
