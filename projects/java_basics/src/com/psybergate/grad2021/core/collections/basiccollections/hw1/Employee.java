package com.psybergate.grad2021.core.collections.basiccollections.hw1;

public class Employee {

    private int employeeNum;

    private String positionName;

    public Employee(int employeeNum, String positionName) {
        this.employeeNum = employeeNum;
        this.positionName = positionName;
    }

    public int getEmployeeNum() {
        return employeeNum;
    }

    @Override
    public String toString() {
        return "Employee{" +
                "employeeNum=" + employeeNum +
                ", positionName='" + positionName + '\'' +
                '}';
    }
}
