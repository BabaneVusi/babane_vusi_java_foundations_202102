package com.psybergate.grad2021.core.collections.basiccollections.hw5;

import java.util.Iterator;

public class Utils {

    public static void main(String[] args) {
        TreeStack treeStack = testTreeStack();
        printTreeStack(treeStack);
    }

    private static TreeStack testTreeStack() {
        TreeStack treeStack = new TreeStack();

        treeStack.add("abc");
        treeStack.add("abb");
        treeStack.add("bbb");
        treeStack.add("ghi");
        treeStack.add("def");
        treeStack.add("wyz");
        treeStack.add("jkl");
        treeStack.pop();
        treeStack.remove("abb");
        treeStack.remove("bbb");
        return treeStack;
    }

    private static void printTreeStack(TreeStack treeStack) {
        for (Iterator iterator = treeStack.iterator(); iterator.hasNext();){
            System.out.println("iterator.next() = " + iterator.next());
        }
    }
}
