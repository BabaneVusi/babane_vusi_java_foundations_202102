package com.psybergate.grad2021.core.collections.basiccollections.hw5;

import java.util.Comparator;

public class AscendingOrder implements Comparator<String> {

    public AscendingOrder() {
    }

    @Override
    public int compare(String o1, String o2) {
        return o1.compareTo(o2);
    }
}
