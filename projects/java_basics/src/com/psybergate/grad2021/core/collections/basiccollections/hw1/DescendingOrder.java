package com.psybergate.grad2021.core.collections.basiccollections.hw1;

import java.util.Comparator;

public class DescendingOrder implements Comparator<Integer> {

    public DescendingOrder() {
    }

    @Override
    public int compare(Integer o1, Integer o2) {
        return o2.compareTo(o1);
    }
}
