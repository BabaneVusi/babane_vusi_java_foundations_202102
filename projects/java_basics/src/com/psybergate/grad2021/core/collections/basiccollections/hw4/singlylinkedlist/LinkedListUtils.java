package com.psybergate.grad2021.core.collections.basiccollections.hw4.singlylinkedlist;

import java.util.Iterator;
import java.util.List;

public class LinkedListUtils {

    public static void main(String[] args) {
        MyLinkedList list = new MyLinkedList();

        list.add("abc");
        list.add("def");
        list.add("ghi");
        list.add("jkl");
        list.add("mno");

        System.out.println("list.size() = " + list.size());
        System.out.println("list.contains(\"ghi\") = " + list.contains("ghi"));
        System.out.println("list.contains(\"ghij\") = " + list.contains("ghij"));

        printList(list);
    }

    private static void printList(List list) {
        for (Iterator iterator = list.iterator(); iterator.hasNext(); ) {
            System.out.println("iterator.next() = " + iterator.next());
        }
    }
}
