package com.psybergate.grad2021.core.collections.basiccollections.hw1;

import java.util.*;

public class EmployeeUtils {

    public static void main(String[] args) {
//        SortedMap map = new TreeMap<>(new AscendingOrder());
        SortedMap map = new TreeMap<>(new DescendingOrder());

        Employee e1 = new Employee(1,"vuyo");
        Employee e2 = new Employee(2,"lisa");
        Employee e5 = new Employee(5,"tshepo");
        Employee e3 = new Employee(3,"lebo");
        Employee e4 = new Employee(4,"thabo");

        map.put(e1.getEmployeeNum(), e1);
        map.put(e2.getEmployeeNum(), e2);
        map.put(e3.getEmployeeNum(), e3);
        map.put(e4.getEmployeeNum(), e4);
        map.put(e5.getEmployeeNum(), e5);

        printEmployees(map);
    }

    private static void printEmployees(SortedMap map) {
        Set keys = map.keySet();
        int i = 0;
        for (final Object key : keys) {
            System.out.println("map:" + i++ + "(" + key + ")  = " + map.get(key));
        }
    }
}
