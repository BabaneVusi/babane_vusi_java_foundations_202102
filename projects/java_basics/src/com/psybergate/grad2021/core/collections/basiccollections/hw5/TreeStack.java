package com.psybergate.grad2021.core.collections.basiccollections.hw5;

import java.util.Comparator;
import java.util.Iterator;

public class TreeStack extends MyAbstractStack {

    private static final int TWO = 2;

    private static final int ZERO = 0;

    private Object[] list = new Object[100];

    private int listCapacity = list.length;

    private int listSize;

    private Comparator comparator;

    public TreeStack() {
    }

    public TreeStack(Comparator comparator) {
        this.comparator = comparator;
    }

    @Override
    public Comparator comparator() {
        return comparator;
    }

    @Override
    public int size() {
        return listSize;
    }

    @Override
    public boolean isEmpty() {
        return size() == ZERO;
    }

    @Override
    public boolean contains(Object o) {
        for (Iterator iterator = iterator(); iterator.hasNext(); ) {
            if (iterator.next().equals(o)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public Iterator iterator() {
        return new MyIterator(list, size());
    }

    @Override
    public Object[] toArray() {
        return new Object[0];
    }

    @Override
    public void push(Object o) {
        add(o);
    }

    private void increaseListSize() {
        Object[] tempList = new Object[listCapacity * TWO];
        for (int i = 0; i < size() - 1; i++) {
            tempList[i] = list[i];
        }
        list = tempList;
    }

    @Override
    public Object pop() {
        if ((listSize - 1) < ZERO) {
            throw new ArrayIndexOutOfBoundsException();
        }

        Object tempObject = list[listSize - 1];
        list[--listSize] = null;
        return tempObject;
    }

    @Override
    public Object get(int position) {
        if (position < ZERO || position > listSize) {
            throw new ArrayIndexOutOfBoundsException();
        }

        return list[position];
    }

    @Override
    public boolean add(Object o) {
        checkListCapacity();

        if (isEmpty()) {
            list[listSize++] = o;
            return true;
        }

        if (comparator == null) {
            Comparable comparableObject = (Comparable) o;
            if (sortByComparable(o, comparableObject)) {
                return true;
            }
        } else {
            if (sortByComparator(o)) {
                return true;
            }
        }

        list[listSize++] = o;
        return true;
    }

    private void checkListCapacity() {
        if (listSize == listCapacity) {
            increaseListSize();
        }
    }

    private boolean sortByComparator(Object o) {
        for (int i = 0; i < listSize; i++) {
            if (comparator.compare(o, list[i]) < 0) {
                shiftElementsUp(i, o);
                return true;
            }
        }
        return false;
    }

    private boolean sortByComparable(Object o, Comparable comparableObject) {
        for (int i = 0; i < listSize; i++) {
            if (comparableObject.compareTo(list[i]) < 0) {
                shiftElementsUp(i, o);
                return true;
            }
        }
        return false;
    }

    private void shiftElementsUp(int i, Object o) {
        for (int j = listSize - 1; j >= i; j--) {
            list[j + 1] = list[j];
        }
        list[i] = o;
        listSize++;
    }

    @Override
    public boolean remove(Object o) {
        if (isEmpty()) {
            return false;
        }

        for (int i = 0; i < listSize; i++) {
            if (list[i].equals(o)) {
                shiftElementsDown(i);
            }
        }

        return true;
    }

    private void shiftElementsDown(int index) {
        for (int i = index; i < listSize; i++){
            list[i] = list[i + 1];
        }
        listSize--;
    }
}
