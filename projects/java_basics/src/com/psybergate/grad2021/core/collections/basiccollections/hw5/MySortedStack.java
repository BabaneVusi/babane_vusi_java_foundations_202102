package com.psybergate.grad2021.core.collections.basiccollections.hw5;

import java.util.Comparator;

public interface MySortedStack extends MyStack {

    public Comparator comparator();

}
