package com.psybergate.grad2021.core.collections.basiccollections.hw2;

import java.util.Comparator;

public class AscendingOrder implements Comparator<Integer> {

    public AscendingOrder() {
    }

    @Override
    public int compare(Integer o1, Integer o2) {
        return o1.compareTo(o2);
    }
}
