package com.psybergate.grad2021.core.reflection.ce2;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

public class ReflectionUtils {

    public static void main(String[] args) throws NoSuchMethodException, IllegalAccessException,
            InvocationTargetException, InstantiationException {
        Class clazz = Student.class;

//        defaultConstructorInstance(clazz);

        Constructor<Student> constructor = clazz.getConstructor(String.class, String.class, int.class,
                String.class, int.class);
        Student student = constructor.newInstance("Vuyo", "Babane", 123456, "EIE", 2000);
        System.out.println("student.getIcamNumber() = " + student.getIcamNumber());
    }

    private static void defaultConstructorInstance(Class clazz) throws NoSuchMethodException, InstantiationException, IllegalAccessException, InvocationTargetException {
        Constructor<Student> constructor = clazz.getConstructor();
        Student student = constructor.newInstance();
        student.setName("Vusi");
        System.out.println("student.getName() = " + student.getName());
    }
}