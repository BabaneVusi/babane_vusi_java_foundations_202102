package com.psybergate.grad2021.core.reflection.ce1;

import java.lang.annotation.*;

@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE})

public @interface DomainClass {

    String name() default "";
}

