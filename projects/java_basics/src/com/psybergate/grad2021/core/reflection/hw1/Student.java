package com.psybergate.grad2021.core.reflection.hw1;

import java.io.Serializable;

@DomainClass(name = "Student")
public class Student implements Serializable {

    public int icamNumber;

    private String name;

    private String surname;

    private int studentNumber;

    private String course;

    public Student() {
    }

    public Student(String name, String surname, int studentNumber, String course) {
        this(name, surname, studentNumber, course, 1234);
    }

    public Student(String name, String surname, int studentNumber, String course, int icamNumber) {
        this.name = name;
        this.surname = surname;
        this.studentNumber = studentNumber;
        this.course = course;
        this.icamNumber = icamNumber;
    }

//    public int randomNumber(int min, int max){
//        Random random = new Random();
//        return random.nextInt((max - min) - 1) + min;
//    }

    private int calculateCourseCost() {
        return 123455;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public int getStudentNumber() {
        return studentNumber;
    }

    public void setStudentNumber(int studentNumber) {
        this.studentNumber = studentNumber;
    }

    public String getCourse() {
        return course;
    }

    public void setCourse(String course) {
        this.course = course;
    }

    public int getIcamNumber() {
        return icamNumber;
    }

    public void setIcamNumber(int icamNumber) {
        this.icamNumber = icamNumber;
    }
}
