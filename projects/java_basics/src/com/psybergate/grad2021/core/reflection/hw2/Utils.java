package com.psybergate.grad2021.core.reflection.hw2;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Properties;

public class Utils {

    public static void main(String[] args) throws IllegalAccessException, InstantiationException, ClassNotFoundException, IOException {
//        Class<List> clazz = List.class;

        ClassLoader classLoader = Utils.class.getClassLoader();

        Properties properties = new Properties();

        InputStream inputStream = classLoader.getResourceAsStream("com/psybergate/grad2021/core/reflection/hw2" +
                "/listtype.txt");

        properties.load(inputStream);

        String listType = properties.getProperty("list");

//        Class objectClass = Class.forName("java.util.ArrayList");

        Class objectClass = Class.forName(listType);

        List list = (List) objectClass.newInstance();

        addMonths(list);
        print(list);
    }

    public static void print(List list) {
        System.out.println("list.getClass() = " + list.getClass());
        for (Object obj : list) {
            System.out.println("obj = " + obj.toString());
        }
    }

    private static void addMonths(List list) {
        list.add("January");
        list.add("February");
        list.add("March");
        list.add("April");
        list.add("May");
        list.add("June");
        list.add("July");
        list.add("August");
        list.add("September");
        list.add("October");
        list.add("November");
        list.add("December");
    }
}
