package com.psybergate.grad2021.core.reflection.hw1;

import java.lang.annotation.Annotation;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;

public class ReflectionUtils {

    public static void main(String[] args) {
        Class clazz = new Student().getClass();

        System.out.println("clazz.getSuperclass() = " + clazz.getSuperclass());
        System.out.println("clazz.getModifiers() = " + clazz.getModifiers());
        getFieldModifiers(clazz);
        getClassMethodModifiers(clazz);
        getClassMethodReturnTypes(clazz);
    }

    private static void getClassMethodReturnTypes(Class clazz) {
        Method[] methods = clazz.getDeclaredMethods();
        for (Method method : methods) {
            System.out.println("method.getModifiers() = " + method + " = " + method.getReturnType());
        }
    }

    private static void getClassMethodModifiers(Class clazz) {
        Method[] methods = clazz.getDeclaredMethods();
        for (Method method : methods) {
            System.out.println("method.getModifiers() = " + method  + " = " + method.getModifiers());
        }
    }

    private static void getClassMethods(Class clazz) {
        Method[] methods = clazz.getMethods();
        for (Method method : methods) {
            System.out.println("method = " + method);
        }
    }

    private static void getClassConstructors(Class clazz) {
        Constructor[] constructors = clazz.getConstructors();
        for (Constructor constructor : constructors) {
            System.out.println("constructor = " + constructor);
        }
    }

    private static void printPublicFields(Class clazz) {
        Field[] fields = clazz.getFields();
        for (Field field : fields) {
            System.out.println("field = " + field);
        }
    }

    private static void getFieldModifiers(Class clazz) {
        Field[] fields = clazz.getDeclaredFields();
        for (Field field : fields) {
            System.out.println("field = " + field + " = " + field.getModifiers());
        }
    }

    private static void printImplementedInterfaces(Class clazz) {
        Class[] interfaces = clazz.getInterfaces();
        for (Class interfase : interfaces) {
            System.out.println("interfase = " + interfase);
        }
    }

    private static void printClassAnnotations(Class clazz) {
        Annotation[] annotations = clazz.getDeclaredAnnotations();
        for (Annotation annotation : annotations) {
            System.out.println("annotation = " + annotation.toString());
        }
    }
}
