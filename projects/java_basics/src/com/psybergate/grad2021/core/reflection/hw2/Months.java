package com.psybergate.grad2021.core.reflection.hw2;

import java.util.List;

public class Months {

    List<String> list;

    public Months() {
        addMonths();
    }

    private void addMonths() {
        list.add("January");
        list.add("February");
        list.add("March");
        list.add("April");
        list.add("May");
        list.add("June");
        list.add("July");
        list.add("August");
        list.add("September");
        list.add("October");
        list.add("November");
        list.add("December");
    }
}
