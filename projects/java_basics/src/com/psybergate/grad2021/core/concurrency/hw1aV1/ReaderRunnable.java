package com.psybergate.grad2021.core.concurrency.hw1aV1;

public class ReaderRunnable implements Runnable {
  @Override
  public void run() {
    MessageReader messageReader = new MessageReader();
    try {
      messageReader.read();
    } catch (InterruptedException e) {
      throw new RuntimeException(e);
    }
  }
}
