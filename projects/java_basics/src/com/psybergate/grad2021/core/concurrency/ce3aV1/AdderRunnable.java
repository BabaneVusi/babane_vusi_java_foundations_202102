package com.psybergate.grad2021.core.concurrency.ce3aV1;

public class AdderRunnable implements Runnable {

  private Adder adder;

  private long sum;

  public AdderRunnable(Adder adder) {
    this.adder = adder;
  }

  @Override
  public void run() {
    sum = adder.add();
  }

  public long getSum() {
    return sum;
  }
}
