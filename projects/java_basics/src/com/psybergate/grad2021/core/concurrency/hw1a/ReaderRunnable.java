package com.psybergate.grad2021.core.concurrency.hw1a;

public class ReaderRunnable implements Runnable {
  @Override
  public void run() {
    MessageReader messageReader = new MessageReader();
    while (true) {
      try {
        System.out.println("Reader");
        messageReader.read();
        Thread.sleep(150);
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
    }
  }
}
