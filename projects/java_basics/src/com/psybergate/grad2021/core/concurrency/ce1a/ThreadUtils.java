package com.psybergate.grad2021.core.concurrency.ce1a;

public class ThreadUtils {

  public static void main(String[] args) {
    Calculator calc = new Calculator();

    MyAdder ma1 = new MyAdder(calc);
    MyAdder ma2 = new MyAdder(calc);

    Thread thread1 = new Thread(ma1, "thread1");
    Thread thread2 = new Thread(ma2, "thread2");

    thread1.start();
    thread2.start();
  }
}
