package com.psybergate.grad2021.core.concurrency.hw1aV1;

import static com.psybergate.grad2021.core.concurrency.hw1aV1.Messages.getReference;
import static com.psybergate.grad2021.core.utils.Utils.randNumberGenerator;

public class MessageWriter {

  public void write() {
//    int counter = 1;
    while (true) {
      try {
//        System.out.println("Writer");
        String message = "Message: " + randNumberGenerator(1, 2000);
        writeMessage(message);
        Thread.sleep(10);
//        if (counter++ == 20) {
//          break;
//        }
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
    }
  }

  public void writeMessage(String message) throws InterruptedException {
    getReference().add(message);
    System.out.println("wrote = " + message);
  }
}
