package com.psybergate.grad2021.core.concurrency.hw1aV1;

import java.util.ArrayList;
import java.util.List;

public class Messages {

  private int counter = 0;

  public static final Messages INSTANCE = new Messages();

  private final List<String> messageStorage = new ArrayList<>();

  public void add(String message) throws InterruptedException {
    counter++;
    synchronized (this.messageStorage) {
      messageStorage.add(message);
      if (counter == 20){
        this.messageStorage.notifyAll();
        this.messageStorage.wait();
      }
    }
  }

  public String getMessage() throws InterruptedException {

    if (messageStorage.size() == 0) {
      return null;
    } else {
      this.messageStorage.wait();
    }

    synchronized (this.messageStorage) {
      if (messageStorage.isEmpty()) {
        this.messageStorage.notifyAll();
        return null;
      }
      return messageStorage.remove(0);
    }
  }

  public synchronized static Messages getReference() {
    return INSTANCE;
  }
}
