package com.psybergate.grad2021.core.concurrency.hw1aV1;

public class MessageReader {

  public void read() throws InterruptedException {
    while (alwaysTrue()) {
//      System.out.println("Reader");
      String message = getMessageInstance().getMessage();
      while (message != null) {
        System.out.println("read = " + message);
        message = getMessageInstance().getMessage();
      }
      Thread.sleep(100);
    }
  }

  private boolean alwaysTrue() {
    return true;
  }

  public Messages getMessageInstance() {
    return Messages.getReference();
  }
}
