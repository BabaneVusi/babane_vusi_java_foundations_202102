package com.psybergate.grad2021.core.concurrency.hw1a;

import static com.psybergate.grad2021.core.concurrency.hw1a.Messages.getReference;

public class MessageWriter {
  public void writeMessage (String message){
    getReference().add(message);
    System.out.println("message = " + message);
  }
}
