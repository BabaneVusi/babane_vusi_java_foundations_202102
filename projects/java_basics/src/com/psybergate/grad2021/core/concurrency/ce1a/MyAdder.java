package com.psybergate.grad2021.core.concurrency.ce1a;

public class MyAdder implements Runnable {

  private Calculator adder;

  public MyAdder(Calculator calculator) {
    this.adder = calculator;
  }

  @Override
  public void run() {
    adder.add();
  }
}
