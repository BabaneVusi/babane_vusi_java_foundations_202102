package com.psybergate.grad2021.core.concurrency.ce4a;

public class Utils {
  public static void main(String[] args) throws InterruptedException {
//    Singleton s1 = Singleton.getInstance();
//    Singleton s2 = Singleton.getInstance();
//
//    System.out.println("(s1 == s2) = " + (s1 == s2)); // Same instance is created

    Work w1 = new Work();
    Work w2 = new Work();
    Thread thread1 = new Thread(w1);
    Thread thread2 = new Thread(w2);
    thread1.start();
    thread2.start();

    Thread.sleep(10);
    System.out.println("(w1.getInstance() == w2.getInstance()) = " + (w1.getInstance() == w2.getInstance()));
  }
}
