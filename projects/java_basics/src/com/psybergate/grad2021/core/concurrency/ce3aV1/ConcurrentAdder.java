package com.psybergate.grad2021.core.concurrency.ce3aV1;

import java.util.HashMap;
import java.util.Map;

public class ConcurrentAdder {

  private int range;

  private int numberOfThreads;

  private int sum;

  public ConcurrentAdder(int range, int numberOfThreads) {
    this.range = range;
    this.numberOfThreads = numberOfThreads;
  }

  public long add() throws InterruptedException {
    long startValue = 1;
    long endValue = 0;
    long interval = range / numberOfThreads;
    Map<Thread, AdderRunnable> threads = new HashMap<>();

    for (int i = 1; i <= numberOfThreads; i++) {
      startValue = endValue + 1;
      if (i == numberOfThreads) {
          endValue = range;
      } else {
        endValue += interval;
      }
      Adder a = new Adder(startValue, endValue);
      AdderRunnable r = new AdderRunnable(a);
      Thread thread = new Thread(r);
      thread.start();
      threads.put(thread, r);
    }

    long sum = 0;
    for (final Map.Entry<Thread, AdderRunnable> entry : threads.entrySet()) {
      Thread thread = entry.getKey();
      AdderRunnable ar = entry.getValue();
      thread.join();
      sum += ar.getSum();
    }
//    thread1.join();

//    Adder a1 = new Adder(startValue, endValue);
//    Adder a2 = new Adder(endValue + 1, range);
//
//    AdderRunnable r1 = new AdderRunnable(a1);
//    AdderRunnable r2 = new AdderRunnable(a2);
//
//    Thread thread1 = new Thread(r1);
//    Thread thread2 = new Thread(r2);
//
//    thread1.start();
//    thread2.start();
//
//    thread1.join();
//    thread2.join();
//
//    sum = (r1.getSum() + r2.getSum());
    return sum;
  }

  public int getSum() {
    return sum;
  }
}
