package com.psybergate.grad2021.core.concurrency.hw2a;

public class Adder {

  private volatile long sum = 0;

  public Adder() {
  }

  public void add(long value){
    System.out.println(Thread.currentThread() + " Start = " + getSum());
    for (long i = 1; i <= value; i++){
      sum += i;
    }
    System.out.println(Thread.currentThread() + " end = " + getSum());
  }

  public long getSum() {
    return sum;
  }
}
