package com.psybergate.grad2021.core.concurrency.demo1;

import static com.psybergate.grad2021.core.concurrency.demo1.Concurrency.startValue;

public class Concurrency {
  public static int startValue = 1_000_000;

  public static void main(String[] args) throws InterruptedException {
    Calculator s1 = new Calculator(startValue);

    Adder a1 = new Adder(s1);
    MyAdder ma2 = new MyAdder(s1);

    Thread thread1 = new Thread(a1);
    Thread thread2 = new Thread(ma2);

    thread1.start();
    thread2.start();

    thread1.join();
    thread2.join();

    System.out.println("a1.getSum() = " + s1.getSum());
  }
}

class Adder extends Thread {

  private Calculator sum;

  Adder(Calculator sum) {
    this.sum = sum;
  }

  @Override
  public void run() {
    sum.add(startValue);
  }

  public Calculator getSum() {
    return sum;
  }
}
