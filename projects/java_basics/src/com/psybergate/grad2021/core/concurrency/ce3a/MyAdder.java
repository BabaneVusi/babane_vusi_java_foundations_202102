package com.psybergate.grad2021.core.concurrency.ce3a;

import static com.psybergate.grad2021.core.concurrency.demo1.Concurrency.startValue;

public class MyAdder implements Runnable {

  private Sum adder;

  public MyAdder(Sum sum) {
    this.adder = sum;
  }

  @Override
  public void run() {
    adder.add(startValue);
  }
}
