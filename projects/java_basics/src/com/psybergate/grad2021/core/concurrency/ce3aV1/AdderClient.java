package com.psybergate.grad2021.core.concurrency.ce3aV1;

public class AdderClient {

  public static void main(String[] args) throws InterruptedException {
    ConcurrentAdder ca1 = new ConcurrentAdder(1_000_000, 9);
    System.out.println("ca1.add() = " + ca1.add());
  }
}
