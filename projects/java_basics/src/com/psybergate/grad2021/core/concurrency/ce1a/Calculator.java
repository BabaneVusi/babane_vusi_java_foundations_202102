package com.psybergate.grad2021.core.concurrency.ce1a;

public class Calculator {

  public void add() {
    subtract();
  }

  private void subtract() {
    multiply();
  }

  private void multiply() {
    divide();
  }

  private void divide() {
    throw new RuntimeException();
  }
}
