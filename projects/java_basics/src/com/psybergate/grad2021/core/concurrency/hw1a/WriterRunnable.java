package com.psybergate.grad2021.core.concurrency.hw1a;

import static com.psybergate.grad2021.core.utils.Utils.randNumberGenerator;

public class WriterRunnable implements Runnable {

  @Override
  public void run() {
    MessageWriter messageWriter = new MessageWriter();
    System.out.println("Writer");
    int counter = 1;
    while (true) {
      try {
        String message = "Message: " + randNumberGenerator(1, 2000);
        messageWriter.writeMessage(message);
        Thread.sleep(15);
        if (counter++ == 50) {
          break;
        }
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
    }
  }
}
