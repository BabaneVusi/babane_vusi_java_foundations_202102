package com.psybergate.grad2021.core.concurrency.ce3aV1;

public class Adder {

  private long sum;

  private long startValue;

  private long endValue;

  public Adder(long startValue, long endValue) {
    this.startValue = startValue;
    this.endValue = endValue;
  }

  public long add() {
    for (long i = startValue; i <= endValue; i++) {
      sum += i;
    }
    return sum;
  }

  public long getSum() {
    return sum;
  }
}
