package com.psybergate.grad2021.core.concurrency.demo1;

import static com.psybergate.grad2021.core.concurrency.demo1.Concurrency.startValue;

public class MyAdder implements Runnable {

  private Calculator adder;

  public MyAdder(Calculator sum) {
    this.adder = sum;
  }

  @Override
  public void run() {
    adder.add(startValue);
  }
}
