package com.psybergate.grad2021.core.concurrency.hw1a;

public class MessageReader {

  public void read() {
    while (true) {
      String message = getMessageInstance().getMessage();
      if (message == null) {
        break;
      }
      System.out.println("message = " + message);
    }
  }

  public Messages getMessageInstance() {
    return Messages.getReference();
  }
}
