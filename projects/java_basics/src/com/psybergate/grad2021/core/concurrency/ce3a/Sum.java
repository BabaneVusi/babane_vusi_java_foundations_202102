package com.psybergate.grad2021.core.concurrency.ce3a;

public class Sum {

  public static final int ONE = 1;

  private int sum;

  public Sum(int sum) {
    this.sum = sum;
  }

//  public synchronized void add(int value) {
//    for (int i = 1; i <= value; i++) {
//      sum += ONE;
//    }
//  }

  public void add(int value) {
    synchronized (this) {
      for (int i = 1; i <= value; i++) {
        sum += ONE;
      }
    }
  }

  public int getSum() {
    return sum;
  }
}
