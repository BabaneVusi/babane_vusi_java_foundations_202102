package com.psybergate.grad2021.core.concurrency.ce4a;

public class Singleton {

  static Singleton instance;

  private Singleton() {
  }

  public static Singleton getInstance() {
    if (instance == null) {
      synchronized (Singleton.class) {
        if (instance == null) {
          instance = new Singleton();
        }
      }
    }
    return instance;
  }
}

class Work implements Runnable {

  private Singleton instance;

  @Override
  public void run() {
    instance = Singleton.getInstance();
  }

  public Singleton getInstance() {
    return instance;
  }
}