package com.psybergate.grad2021.core.concurrency.hw2a;

public class AdderUtils {

  public static final int END_VALUE = 1_000 ;

  public static void main(String[] args) throws InterruptedException {
    Adder a1 = new Adder();
    AdderRunnable ar1 = new AdderRunnable(a1, END_VALUE);
    AdderRunnable ar2 = new AdderRunnable(a1, END_VALUE);
    AdderRunnable ar3 = new AdderRunnable(a1, END_VALUE);
    Thread thread1 = new Thread(ar1);
    Thread thread2 = new Thread(ar2);
    Thread thread3 = new Thread(ar2);
    thread1.start();
    thread2.start();
    thread3.start();
    thread1.join();
    thread2.join();
    thread3.join();
    System.out.println("a1.getSum() = " + a1.getSum());
  }
}
