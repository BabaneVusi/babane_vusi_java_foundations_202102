package com.psybergate.grad2021.core.concurrency.hw1aV1;

public class WriterRunnable implements Runnable {

  @Override
  public void run() {
    MessageWriter messageWriter = new MessageWriter();
    messageWriter.write();
  }
}
