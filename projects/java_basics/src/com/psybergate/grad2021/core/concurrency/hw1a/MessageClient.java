package com.psybergate.grad2021.core.concurrency.hw1a;

public class MessageClient {

  public static void main(String[] args) {
    MessageWriter mw = new MessageWriter();
    MessageReader mr = new MessageReader();

    WriterRunnable wr1 = new WriterRunnable();
    ReaderRunnable rr1 = new ReaderRunnable();

    Thread thread1 = new Thread(wr1);
    Thread thread2 = new Thread(rr1);

    thread1.start();
    thread2.start();

  }
}
