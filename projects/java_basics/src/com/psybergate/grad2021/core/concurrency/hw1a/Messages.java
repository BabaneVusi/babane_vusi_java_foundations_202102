package com.psybergate.grad2021.core.concurrency.hw1a;

import java.util.ArrayList;
import java.util.List;

public class Messages {

  public static final Messages INSTANCE = new Messages();

  List<String> messageStorage = new ArrayList<>();

  public void add(String message) {
    messageStorage.add(message);
  }

  public String getMessage() {
    if (messageStorage.isEmpty()){
      return null;
    }
    return messageStorage.remove(0);
  }

  public static Messages getReference() {
    return INSTANCE;
  }
}
