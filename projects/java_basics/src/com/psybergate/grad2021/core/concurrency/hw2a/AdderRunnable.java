package com.psybergate.grad2021.core.concurrency.hw2a;

public class AdderRunnable implements Runnable {

  private Adder adder;

  private long endValue;

  public AdderRunnable(Adder adder, long endValue) {
    this.adder = adder;
    this.endValue = endValue;
  }

  @Override
  public void run() {
    System.out.println("Thread " + Thread.currentThread() + " sum = " + adder.getSum());
    adder.add(endValue);
    System.out.println("Thread " + Thread.currentThread() + " sum = " + adder.getSum());
  }

  public long getCalcSum() {
    return adder.getSum();
  }
}
