package com.psybergate.grad2021.jeefnds.servlets.hwweb3.servlets;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.Statement;

public class Customer {

  public void add(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

    PrintWriter out = resp.getWriter();
    try {
      Connection connection = DatabaseConnection.createConnection();
      Statement statement = connection.createStatement();
      statement.execute(getSqlStatement(req));
      out.println("customer added successfully!");
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  private String getSqlStatement(HttpServletRequest req) {
    String customerNum = req.getParameter("customernum");
    String customerName = req.getParameter("name");
    String customerSurname = req.getParameter("surname");
    String dateOfBirth = req.getParameter("birthdate");

    String sql = "insert into jee_customers values(" + customerNum + ",'" + customerName + "','" + customerSurname
        + "','" + dateOfBirth + "');";
    return sql;
  }
}
