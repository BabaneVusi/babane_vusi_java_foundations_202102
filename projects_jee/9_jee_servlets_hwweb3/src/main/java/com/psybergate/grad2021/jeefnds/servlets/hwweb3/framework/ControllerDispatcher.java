package com.psybergate.grad2021.jeefnds.servlets.hwweb3.framework;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Method;
import java.util.Map;
import java.util.Properties;

@WebServlet(value = "/persist/*")
public class ControllerDispatcher extends HttpServlet {

  private Properties properties;

  @Override
  public void init() throws ServletException {
    try {
      InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream(
          "controllers.properties");
      properties = new Properties();
      properties.load(inputStream);
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  @Override
  protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

    String requestType = req.getPathInfo();
    for (Map.Entry<Object, Object> entry : properties.entrySet()) {
      if (entry.getKey().equals(requestType)) {
        try {
          Class classObject = Class.forName((String) entry.getValue());
          Method method = classObject.getMethod("add", HttpServletRequest.class, HttpServletResponse.class);
          method.invoke(classObject.newInstance(), req, resp);
        } catch (Exception e) {
          throw new RuntimeException(e);
        }
      }
    }
  }
}
