package com.psybergate.grad2021.jeefnds.enterprise.hwent2.controller;

import com.psybergate.grad2021.jeefnds.enterprise.hwent2.service.CustomerService;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.time.LocalDate;

public class CustomerController{

  public void addCustomer(HttpServletRequest request, HttpServletResponse response) {
    try {
      RequestDispatcher requestDispatcher = request.getRequestDispatcher("/WEB-INF/views/addCustomer.jsp");
      requestDispatcher.forward(request, response);
    } catch (Exception e) {
      throw new RuntimeException(e.getMessage(), e);
    }
  }

  public void save(HttpServletRequest request, HttpServletResponse response) {
    try {
      long customerNum = Long.parseLong(request.getParameter("customerNum"));
      String name = request.getParameter("name");
      String surname = request.getParameter("surname");
      LocalDate dateOfBirth = LocalDate.parse(request.getParameter("dateOfBirth"));
      Customer customer = new Customer(customerNum, name, surname, dateOfBirth);

      Audit audit = new Audit(LocalDate.now(), "Successfully added customer");
      CustomerService.save(customer, audit);
      RequestDispatcher requestDispatcher = request.getRequestDispatcher("/WEB-INF/views/success.jsp");
      requestDispatcher.forward(request, response);
    } catch (Exception e) {
      throw new RuntimeException(e);
    }
  }
}
