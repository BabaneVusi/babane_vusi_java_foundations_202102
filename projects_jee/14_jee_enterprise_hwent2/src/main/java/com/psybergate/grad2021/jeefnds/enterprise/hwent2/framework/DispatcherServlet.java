package com.psybergate.grad2021.jeefnds.enterprise.hwent2.framework;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Method;
import java.util.Properties;

@WebServlet("/dispatch/*")
public class DispatcherServlet extends HttpServlet {

  private Properties props = new Properties();

  @Override
  public void init() {
    loadControllers();
  }

  private void loadControllers() {
    try {
      InputStream stream =
              DispatcherServlet.class.getClassLoader().getResourceAsStream("controllers" +
                      ".properties");
      props.load(stream);
    } catch (Exception e) {
      throw new RuntimeException(e);
    }
  }

  @Override
  protected void doGet(HttpServletRequest request, HttpServletResponse response) {
    try {
      String pathInfo = request.getPathInfo().substring(1);
      System.out.println(pathInfo);
      String classAndMethod = props.getProperty(pathInfo);
      int index = classAndMethod.indexOf('#');
      String className = classAndMethod.substring(0, index);
      String methodName = classAndMethod.substring(index + 1);
      Object controller = Class.forName(className).newInstance();
      Method method = controller.getClass().getMethod(methodName, HttpServletRequest.class, HttpServletResponse.class);
      method.invoke(controller, request, response);
    } catch (Exception e) {
      throw new RuntimeException(e);
    }
  }

  @Override protected void doPost(HttpServletRequest req, HttpServletResponse resp)
          throws ServletException, IOException {
    try {
      String pathInfo = req.getPathInfo().split("/")[1];
      String classAndMethod = props.getProperty("addCustomer");
      int index = classAndMethod.indexOf('#');
      String className = classAndMethod.substring(0, index);
      String methodName = pathInfo;
      Object controller = Class.forName(className).newInstance();
      Method method = controller.getClass().getMethod(methodName, HttpServletRequest.class, HttpServletResponse.class);
      method.invoke(controller, req, resp);
    } catch (Exception e) {
      throw new RuntimeException(e);
    }
  }
}
