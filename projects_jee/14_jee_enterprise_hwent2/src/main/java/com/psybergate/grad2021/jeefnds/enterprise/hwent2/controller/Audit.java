package com.psybergate.grad2021.jeefnds.enterprise.hwent2.controller;

import javax.persistence.*;
import java.time.LocalDate;

@Entity(name="audit")
public class Audit {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private long id;

  @Column
  private LocalDate auditDate;

  @Column
  private String status;

  public Audit(LocalDate auditDate, String status) {
    this.auditDate = auditDate;
    this.status = status;
  }

  public long getId() {
    return id;
  }

  public LocalDate getAuditDate() {
    return auditDate;
  }

  public String getStatus() {
    return status;
  }
}