package com.psybergate.grad2021.jeefnds.enterprise.hwent2.service;

import com.psybergate.grad2021.jeefnds.enterprise.hwent2.controller.Audit;
import com.psybergate.grad2021.jeefnds.enterprise.hwent2.controller.Customer;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class CustomerService {

  public static final EntityManagerFactory emf;

  static {
    emf = Persistence.createEntityManagerFactory("CustomerAudit_JPA");
  }

  private static EntityManager entityManager;

  public static void save(Customer customer, Audit audit) {
    entityManager = emf.createEntityManager();
    entityManager.getTransaction().begin();
    entityManager.persist(customer);
    entityManager.persist(audit);
    entityManager.getTransaction().commit();
    entityManager.close();
//    emf.close();
  }
}