package com.psybergate.grad2021.jeefnds.enterprise.hwent2.client.service.impl;

import com.psybergate.grad2021.jeefnds.enterprise.hwent2.client.domain.Audit;
import com.psybergate.grad2021.jeefnds.enterprise.hwent2.client.domain.Customer;
import com.psybergate.grad2021.jeefnds.enterprise.hwent2.client.resource.PersistCustomer;
import com.psybergate.grad2021.jeefnds.enterprise.hwent2.client.service.CustomerService;

import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;
import java.time.LocalDate;

@RequestScoped
@TransactionManagement(TransactionManagementType.CONTAINER)
public class CustomerServiceImpl implements CustomerService {

  @Inject
  private PersistCustomer persistCustomer;

  public CustomerServiceImpl() {
  }

  @Transactional(Transactional.TxType.REQUIRED)
  public void save(Customer customer) {
    try {
      Audit audit = new Audit(LocalDate.now(), customer.getCustomerNum());
      persistCustomer.persist(customer, audit);
    } catch (Exception e) {
      throw new RuntimeException(e);
    }
  }
}