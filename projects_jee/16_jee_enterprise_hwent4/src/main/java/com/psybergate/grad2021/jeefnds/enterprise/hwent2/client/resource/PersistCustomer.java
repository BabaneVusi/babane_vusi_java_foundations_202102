package com.psybergate.grad2021.jeefnds.enterprise.hwent2.client.resource;

import com.psybergate.grad2021.jeefnds.enterprise.hwent2.client.domain.Audit;
import com.psybergate.grad2021.jeefnds.enterprise.hwent2.client.domain.Customer;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Named
@ApplicationScoped
public class PersistCustomer {

  @PersistenceContext(unitName = "CustomerAudit_JPA")
  private EntityManager entityManager;

  public PersistCustomer() {
  }

  public void persist(Customer customer, Audit audit){
    try {
      entityManager.persist(customer);
      entityManager.persist(audit);
    } catch (Exception e) {
      throw new RuntimeException(e);
    }
  }
}
