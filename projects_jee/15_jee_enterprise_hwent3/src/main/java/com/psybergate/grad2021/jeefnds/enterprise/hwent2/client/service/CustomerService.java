package com.psybergate.grad2021.jeefnds.enterprise.hwent2.client.service;

import com.psybergate.grad2021.jeefnds.enterprise.hwent2.client.domain.Customer;

import javax.ejb.Local;

@Local
public interface CustomerService {

  public void save (Customer customer);
}
