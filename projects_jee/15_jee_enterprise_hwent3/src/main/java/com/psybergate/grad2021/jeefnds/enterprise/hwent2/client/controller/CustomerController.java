package com.psybergate.grad2021.jeefnds.enterprise.hwent2.client.controller;

import com.psybergate.grad2021.jeefnds.enterprise.hwent2.client.domain.Customer;
import com.psybergate.grad2021.jeefnds.enterprise.hwent2.client.service.CustomerService;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.time.LocalDate;

@Named
@ApplicationScoped
public class CustomerController{

  @Inject
  private CustomerService customerService;

  public CustomerController() {
  }

  public void addCustomer(HttpServletRequest request, HttpServletResponse response) {
    try {
      RequestDispatcher requestDispatcher = request.getRequestDispatcher("/WEB-INF/views/addCustomer.jsp");
      requestDispatcher.forward(request, response);
    } catch (Exception e) {
      throw new RuntimeException(e.getMessage(), e);
    }
  }

  public void save(HttpServletRequest request, HttpServletResponse response) {
    try {
      Customer customer = getCustomer(request);
      customerService.save(customer);
      RequestDispatcher requestDispatcher = request.getRequestDispatcher("/WEB-INF/views/success.jsp");
      requestDispatcher.forward(request, response);
    } catch (Exception e) {
      throw new RuntimeException(e);
    }
  }

  private Customer getCustomer(HttpServletRequest request) {
    long customerNum = Long.parseLong(request.getParameter("customerNum"));
    String name = request.getParameter("name");
    String surname = request.getParameter("surname");
    LocalDate dateOfBirth = LocalDate.parse(request.getParameter("dateOfBirth"));
    return new Customer(customerNum, name, surname, dateOfBirth);
  }
}
