package com.psybergate.grad2021.jeefnds.enterprise.hwent2.framework;

import com.psybergate.grad2021.jeefnds.enterprise.hwent2.client.controller.CustomerController;

import javax.enterprise.inject.spi.CDI;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/dispatch/*")
public class DispatcherServlet extends HttpServlet {

  private final CustomerController customerController = CDI.current().select(CustomerController.class).get();

  @Override
  protected void doGet(HttpServletRequest request, HttpServletResponse response) {
    customerController.addCustomer(request, response);
  }

  @Override
  protected void doPost(HttpServletRequest req, HttpServletResponse resp)
      throws ServletException, IOException {
    customerController.save(req, resp);
  }
}
