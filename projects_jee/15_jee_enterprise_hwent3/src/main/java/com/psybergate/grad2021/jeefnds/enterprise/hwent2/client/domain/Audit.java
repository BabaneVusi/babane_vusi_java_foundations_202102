package com.psybergate.grad2021.jeefnds.enterprise.hwent2.client.domain;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
public class Audit {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private long id;

  @Column
  private LocalDate auditDate;

  @Column
  private long customerNum;

  public Audit() {
  }

  public Audit(LocalDate auditDate, long customerNum) {
    this.auditDate = auditDate;
    this.customerNum = customerNum;
  }

  public long getId() {
    return id;
  }

  public LocalDate getAuditDate() {
    return auditDate;
  }

  public long getCustomerNum() {
    return customerNum;
  }
}