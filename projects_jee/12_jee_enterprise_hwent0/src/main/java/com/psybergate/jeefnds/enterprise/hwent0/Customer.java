package com.psybergate.jeefnds.enterprise.hwent0;

import java.time.LocalDate;

public class Customer {

  private int customerNum;

  private String name;

  private String surname;

  private LocalDate birthDate;

  public Customer(int customerNum, String name, String surname, LocalDate birthDate) {
    this.customerNum = customerNum;
    this.name = name;
    this.surname = surname;
    this.birthDate = birthDate;
  }

  public int getCustomerNum() {
    return customerNum;
  }

  public String getName() {
    return name;
  }

  public String getSurname() {
    return surname;
  }

  public LocalDate getBirthDate() {
    return birthDate;
  }
}
