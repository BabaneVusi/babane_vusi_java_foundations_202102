package com.psybergate.jeefnds.enterprise.hwent0;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class Jdbc {

  private Connection createConnection() {
    Connection connection = null;
    try {
      Class.forName("org.postgresql.Driver");
      connection = DriverManager
          .getConnection("jdbc:postgresql://localhost:5432/customers",
              "postgres", "admin");
      System.out.println("Opened database successfully");
    } catch (Exception e) {
      e.printStackTrace();
      System.err.println(e.getClass().getName() + ": " + e.getMessage());
      System.exit(0);
    }
    return connection;
  }

  public void insertCustomer(Customer customer, Audit audit) throws SQLException {
      Connection conn;
    try {
      conn = createConnection();
      conn.setAutoCommit(false);
      // Insert a customer
      String customerSql = "insert into jee_customers values(" + customer.getCustomerNum() + ",'" + customer.getName()
      + "','" + customer.getSurname() + "','" + customer.getBirthDate() + "');";
      Statement statement0 = conn.createStatement();
      statement0.execute(customerSql);
      // Insert audit
      String auditSql = "insert into audits values(" + customer.getCustomerNum() + ",'" + audit.getCurrentDate() + "');";
      Statement statement1 = conn.createStatement();
      statement1.execute(auditSql);
      conn.commit();
      System.out.println("persisted customer and audit successfully");
    } catch (Exception e) {
      throw new RuntimeException(e);
    }
  }
}
