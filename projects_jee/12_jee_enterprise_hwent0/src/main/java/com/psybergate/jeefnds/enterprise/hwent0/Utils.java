package com.psybergate.jeefnds.enterprise.hwent0;

import java.sql.SQLException;
import java.time.LocalDate;

public class Utils {

  public static void main(String[] args) throws SQLException {
    Jdbc jdbc = new Jdbc();
    Customer c0 = new Customer(7, "vusi", "bee", LocalDate.of(2002,5,19));
    jdbc.insertCustomer(c0, new Audit());
  }
}
