package com.psybergate.grad2021.jeefnds.servlets.arch1.ce5a;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Takes in a name from the web request.
 * And return hello with the passed in name.
 */
@WebServlet(value = "/greet")
public class GreetingServlet extends HttpServlet {

  @Override protected void doGet(HttpServletRequest req, HttpServletResponse resp)
      throws ServletException, IOException {
    resp.setContentType("text/html");
    PrintWriter out = resp.getWriter();
    String name = req.getParameter("name");

    out.println("<html><body>");
    if (name == null) {
      out.println("Name was not specified");
    } else {
      out.println("Hello " + name);
    }
    out.println("</body></html>");
  }
}
