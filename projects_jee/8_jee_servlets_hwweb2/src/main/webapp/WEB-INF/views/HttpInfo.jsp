<%@ page import="java.util.Enumeration"%>

<html>
    <body>
       <h1> http header names and values : </h1>
        <%
            Enumeration<String> headerNames = request.getHeaderNames();
                while(headerNames.hasMoreElements()){
                  String headerName = headerNames.nextElement();
        %>
            <l1><%= "header name -> " + headerName + "value -> " + request.getHeader(headerName) + "<br>" %></l1>
        <% } %>

        <h1> protocol : </h1>
        <p> <%= request.getProtocol() %> </p>

        <h1> method : </h1>
        <p> <%= request.getMethod() %> </p>

        <h1> request uri : </h1>
        <p> <%= request.getRequestURI() %> </p>

        <h1> pathinfo : </h1>
        <p> <%= request.getPathInfo() %> </p>
    </body>
</html>