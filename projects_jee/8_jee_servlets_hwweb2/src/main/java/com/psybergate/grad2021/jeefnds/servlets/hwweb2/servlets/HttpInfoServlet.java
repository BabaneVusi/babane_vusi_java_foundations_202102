package com.psybergate.grad2021.jeefnds.servlets.hwweb2.servlets;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Enumeration;

@WebServlet(value = "/httpinfo/*")
public class HttpInfoServlet extends HttpServlet {

  @Override
  protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
    PrintWriter out = resp.getWriter();

    out.println("<html><body>");
    out.println("<h2> http header names and values : </h2>");
    Enumeration<String> headerNames = req.getHeaderNames();
    while(headerNames.hasMoreElements()){
      String headerName = headerNames.nextElement();
        out.println("header name -> " + headerName + "value -> " + req.getHeader(headerName) + "<br>");
    }
    out.println("<h2> protocol : </h2>");
    out.println(req.getProtocol());
    out.println("<h2> http method : </h2>");
    out.println(req.getMethod());
    out.println("<h2> request uir : </h2>");
    out.println(req.getRequestURI());
    out.println("<h2> pathinfo : </h2>");
    out.println(req.getPathInfo());
    out.println("</body></html>");
  }
}
