package com.psybergate.jeefnds.servlets.arch2.ce5b;

public class HelloWorldController implements Controller {

  public static final Controller HELLO_WORLD = new HelloWorldController();

  @Override
  public String execute() {
    return ("Hello World");
  }
}
