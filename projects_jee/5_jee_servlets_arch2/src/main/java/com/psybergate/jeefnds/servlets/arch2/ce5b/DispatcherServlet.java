package com.psybergate.jeefnds.servlets.arch2.ce5b;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

@WebServlet(value = "/dispatcher/*")
public class DispatcherServlet extends HttpServlet {

  Map<Object, Object> controllers = new HashMap<>();

  /**
   * Offers some type of an HTML template.
   * The controllers response if written in the html and returned as the servlet response.
   * Controllers are loaded via reflection from the properties file.
   * Each controllers has must OVERRIDE the execute method, which when called by the doGet method,
   * the method of the controller is executed.
   *
   * @param req
   * @param resp
   * @throws ServletException
   * @throws IOException
   */
  @Override
  protected void doGet(HttpServletRequest req, HttpServletResponse resp)
      throws ServletException, IOException {
    resp.setContentType("text/html");
    PrintWriter out = resp.getWriter();
    out.println("<html><body>");
    String requestType = req.getPathInfo();

    InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream("controllers.properties");
    Properties properties = new Properties();
    properties.load(inputStream);

    for (Map.Entry<Object, Object> entry : properties.entrySet()) {
      if (requestType.equals(entry.getKey())) {
        try {
          Class classObject = Class.forName((String) entry.getValue());
          Object instance = classObject.newInstance();
          Method method = classObject.getMethod("execute");
          out.println(method.invoke(instance));
          break;
        } catch (Exception e) {
          throw new RuntimeException(e);
        }
      } else if (!properties.containsKey(requestType)) {
        out.println("Invalid request");
        break;
      }
    }
    out.println("</body></html>");
  }
}
