package com.psybergate.jeefnds.servlets.arch2.ce5b;

import java.time.LocalDate;

public class GetCurrentDateController implements Controller {

  public static final Controller GET_CURRENT_DATE = new GetCurrentDateController();

  @Override
  public String execute() {
    return ("LocalDate.now() = " + LocalDate.now());
  }
}
