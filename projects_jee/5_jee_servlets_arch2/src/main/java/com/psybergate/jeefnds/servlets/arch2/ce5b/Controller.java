package com.psybergate.jeefnds.servlets.arch2.ce5b;

/**
 * Controllers that implement this interface must execute their request inside the execute method.
 * And return the response as a String
 */
public interface Controller {

  public String execute();
}
