package com.psybergate.grad2021.jeefnds.servlets.hwweb6;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.io.PrintWriter;

@WebFilter("/greet")
public class MyFilter implements Filter {

  @Override public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse,
                                 FilterChain filterChain) throws IOException, ServletException {
    HttpServletRequest request = (HttpServletRequest) servletRequest;
    PrintWriter pw = servletResponse.getWriter();
    String name = request.getParameter("name");

    if (authenticateUser(name)) {
      filterChain.doFilter(servletRequest, servletResponse);
    } else {
      servletResponse.setContentType("text/html");
      pw.println("<html><body><h2>Access denied</h2></body></html>");
      pw.close();
    }
  }

  private boolean authenticateUser(String name) {
    return !name.equalsIgnoreCase("john");
  }

  @Override public void destroy() {
    Filter.super.destroy();
  }
}
