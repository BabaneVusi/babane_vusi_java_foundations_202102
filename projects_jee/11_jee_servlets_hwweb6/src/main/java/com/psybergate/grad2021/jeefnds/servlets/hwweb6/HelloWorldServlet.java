package com.psybergate.grad2021.jeefnds.servlets.hwweb6;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet("/greet/*")
public class HelloWorldServlet extends HttpServlet {

  @Override
  protected void doGet(HttpServletRequest req, HttpServletResponse resp)
      throws ServletException, IOException {
    resp.setContentType("text/html");
    String name = req.getParameter("name");
    PrintWriter pw = resp.getWriter();
    greet(name, pw);
  }

  private void greet(String name, PrintWriter pw) {
    pw.println("<html><body>");
    pw.println("<h2>Hello " + name + "</h2>");
    pw.println("</body></html>");
  }
}
