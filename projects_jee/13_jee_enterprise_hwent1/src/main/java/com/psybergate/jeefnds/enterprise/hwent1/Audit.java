package com.psybergate.jeefnds.enterprise.hwent1;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.LocalDate;

@Entity
@Table(name = "audits")
public class Audit {

  @Id
  @Column(name = "customernum")
  private Long customerNum;

  @Column(name = "auditdate")
  private LocalDate auditDate;

  public Audit() {
  }

  public Audit(Long customerNum) {
    this.customerNum = customerNum;
    this.auditDate = LocalDate.now();
  }
}
