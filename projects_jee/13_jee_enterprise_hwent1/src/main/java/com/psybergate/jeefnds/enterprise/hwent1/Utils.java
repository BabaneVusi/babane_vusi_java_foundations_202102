package com.psybergate.jeefnds.enterprise.hwent1;

import javax.persistence.Persistence;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityManager;
import java.time.LocalDate;

public class Utils {


  public static void main(String[] args) {
    EntityManagerFactory emf = Persistence.createEntityManagerFactory("org.hibernate.jpa");

    LocalDate date = LocalDate.of(2021, 12, 12);

    Long customerNum = 56L;
    Customer customer = new Customer(customerNum, "Donald", "Pudi", date);
    Audit audit = new Audit(customerNum);

    EntityManager entityManager = emf.createEntityManager();
    entityManager.getTransaction().begin();
    entityManager.persist(customer);
    entityManager.persist(audit);

    entityManager.getTransaction().commit();
    entityManager.close();
  }
}
