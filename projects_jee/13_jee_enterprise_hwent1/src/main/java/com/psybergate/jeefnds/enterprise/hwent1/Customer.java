package com.psybergate.jeefnds.enterprise.hwent1;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.LocalDate;

@Entity
@Table(name = "jee_customers")
public class Customer {

  @Id
  @Column(name = "customernum")
  private Long customerNum;

  @Column(name = "name")
  private String name;

  @Column(name = "surname")
  private String surname;

  @Column(name = "birthdate")
  private LocalDate birthDate;

  public Customer() {
  }

  public Customer(Long customerNum, String name, String surname, LocalDate birthDate) {
    this.customerNum = customerNum;
    this.name = name;
    this.surname = surname;
    this.birthDate = birthDate;
  }

  public void setCustomerNum(Long customerNum) {
    this.customerNum = customerNum;
  }

  public void setName(String name) {
    this.name = name;
  }

  public void setSurname(String surname) {
    this.surname = surname;
  }

  public void setBirthDate(LocalDate birthDate) {
    this.birthDate = birthDate;
  }
}
