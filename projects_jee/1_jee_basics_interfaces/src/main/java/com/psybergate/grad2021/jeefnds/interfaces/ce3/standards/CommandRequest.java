package main.java.com.psybergate.grad2021.jeefnds.interfaces.ce3.standards;

import com.sun.media.sound.InvalidDataException;

public interface CommandRequest {

  public void setCommand(String command) throws InvalidDataException;

  public default String getCommand() {
    return null;
  }

  public default int getNum1(){
    return 0;
  }

  public default int getNum2() {
    return 0;
  }
}
