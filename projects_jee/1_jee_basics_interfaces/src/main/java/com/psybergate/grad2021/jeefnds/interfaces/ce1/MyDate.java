package main.java.com.psybergate.grad2021.jeefnds.interfaces.ce1;

import com.sun.media.sound.InvalidDataException;

import java.time.LocalDate;

public interface MyDate {

  /**
   * Takes in number of days and adds on the current date. Then returns the new date.
   * @param days
   * @return
   */
  public LocalDate addDays(int days) throws InvalidDataException;

  public boolean isLeapYear();
}
