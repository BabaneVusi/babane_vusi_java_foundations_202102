package main.java.com.psybergate.grad2021.jeefnds.interfaces.ce1v1.vendorimpl2;

import main.java.com.psybergate.grad2021.jeefnds.interfaces.ce1v1.standard.Date;
import main.java.com.psybergate.grad2021.jeefnds.interfaces.ce1v1.standard.DateFactory;
import main.java.com.psybergate.grad2021.jeefnds.interfaces.ce1v1.standard.InvalidDataException;

public class DateFactoryTwo implements DateFactory {
  @Override
  public Date createDate(int year, int month, int day) throws InvalidDataException {
    return new VendorTwoDate(1999, 04, 18);
  }

//  @Override
//  public Date createDate(int numOfDays) throws InvalidDataException {
//    return null;
//  }
}
