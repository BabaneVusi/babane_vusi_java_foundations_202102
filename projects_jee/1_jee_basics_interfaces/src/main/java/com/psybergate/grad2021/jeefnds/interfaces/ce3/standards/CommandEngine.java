package main.java.com.psybergate.grad2021.jeefnds.interfaces.ce3.standards;

public interface CommandEngine {

  public CommandResponse processCommand (CommandRequest commandRequest);
}
