package main.java.com.psybergate.grad2021.jeefnds.interfaces.ce1v1.vendorimpl1;

import main.java.com.psybergate.grad2021.jeefnds.interfaces.ce1v1.standard.Date;
import main.java.com.psybergate.grad2021.jeefnds.interfaces.ce1v1.standard.DateFactory;
import main.java.com.psybergate.grad2021.jeefnds.interfaces.ce1v1.standard.InvalidDataException;

public class DateFactoryOne implements DateFactory {
  @Override
  public Date createDate(int year, int month, int day) throws InvalidDataException {
    return new VendorOneDate(year,month, day);
  }

//  @Override
//  public Date createDate(int numOfDays) throws InvalidDataException {
//    return new VendorOneDate().addDays(numOfDays);
//  }
}
