package main.java.com.psybergate.grad2021.jeefnds.interfaces.ce3.vendorimpl1;

import main.java.com.psybergate.grad2021.jeefnds.interfaces.ce3.standards.Command;
import main.java.com.psybergate.grad2021.jeefnds.interfaces.ce3.standards.CommandRequest;
import main.java.com.psybergate.grad2021.jeefnds.interfaces.ce3.standards.CommandResponse;

public class MyCommand implements Command {
  @Override
  public CommandResponse execute(CommandRequest commandRequest) {
    MyCommandResponse commandResponse = new MyCommandResponse();
    if (commandRequest.getCommand().equals("current_date")){
      commandResponse.setCurrentDate();
      return commandResponse;
    } else if (commandRequest.getCommand().equals("sum")){
      commandResponse.setSum(commandRequest.getNum1(), commandRequest.getNum2());
      return commandResponse;
    }
      commandResponse.setFactorialOf(commandRequest.getNum1());
      return commandResponse;
  }
}
