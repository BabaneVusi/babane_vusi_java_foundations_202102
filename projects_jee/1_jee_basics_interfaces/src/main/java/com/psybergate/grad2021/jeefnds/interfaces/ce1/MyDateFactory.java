package main.java.com.psybergate.grad2021.jeefnds.interfaces.ce1;

import com.sun.media.sound.InvalidDataException;

import java.time.LocalDate;

public class MyDateFactory implements MyDate {

  private int day;

  private int month;

  private int year;

  LocalDate localDate = LocalDate.now();

  public MyDateFactory() {
    this.day = LocalDate.now().getDayOfMonth();
    this.month = LocalDate.now().getMonthValue();
    this.year = LocalDate.now().getYear();
  }


  @Override
  public LocalDate addDays(int days) throws InvalidDataException {
    int tempDays = day, tempMonth = month + 1, tempYear = year;
    if (days >= 1) {
      tempDays += days;
      while (tempDays > 31) {
        tempDays = tempDays - 31;
        tempMonth += 1;
        if (tempMonth > 12) {
          tempMonth -= 12;
          tempYear += 1;
        }
      }
    } else {
      throw new InvalidDataException("Days must be at least greater than one");
    }
    return LocalDate.of(tempYear, tempMonth, tempDays);
  }

  @Override
  public boolean isLeapYear() {
    return false;
  }

  public int getDay() {
    return day;
  }

  public int getMonth() {
    return month;
  }

  public int getYear() {
    return year;
  }
}
