package main.java.com.psybergate.grad2021.jeefnds.interfaces.ce1v1.standard;

public class InvalidDataException extends Exception {

  public InvalidDataException(String message) {
    super(message);
  }
}
