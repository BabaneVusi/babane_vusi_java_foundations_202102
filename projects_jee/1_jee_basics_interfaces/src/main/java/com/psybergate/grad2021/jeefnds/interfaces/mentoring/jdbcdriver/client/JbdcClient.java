package main.java.com.psybergate.grad2021.jeefnds.interfaces.mentoring.jdbcdriver.client;

import main.java.com.psybergate.grad2021.jeefnds.interfaces.mentoring.jdbcdriver.vendorimpl.MyConnection;
import main.java.com.psybergate.grad2021.jeefnds.interfaces.mentoring.jdbcdriver.vendorimpl.MyStatement;
import main.java.com.psybergate.grad2021.jeefnds.interfaces.mentoring.jdbcdriver.vendorimpl.MyResultSet;

import java.sql.*;

public class JbdcClient {

  public static final String URL = "";

  public static final String USER = "";

  public static final String PASSWORD = "";

  public static void main(String[] args) throws ClassNotFoundException, SQLException {
    Class.forName("main.java.com.psybergate.grad2021.jeefnds.interfaces.mydatabase.jdbcdriver.MyDriver");
    MyConnection connection = (MyConnection) DriverManager.getConnection(URL, USER, PASSWORD);
    MyStatement statement = (MyStatement) connection.createStatement();
    MyResultSet resultSet = (MyResultSet) statement.executeQuery("select * from my_table");
  }
}
