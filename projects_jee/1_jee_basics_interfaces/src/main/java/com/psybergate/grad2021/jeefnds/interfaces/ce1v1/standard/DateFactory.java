package main.java.com.psybergate.grad2021.jeefnds.interfaces.ce1v1.standard;

public interface DateFactory {
  /**
   * Creates a Date
   * @return new Date
   * @throws InvalidDataException if Date is invalid
   */
//  @Deprecated
  public Date createDate(int year, int month, int day) throws InvalidDataException;
}
