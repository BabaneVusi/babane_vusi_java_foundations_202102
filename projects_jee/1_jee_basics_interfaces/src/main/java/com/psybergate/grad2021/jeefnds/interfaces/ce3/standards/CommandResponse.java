package main.java.com.psybergate.grad2021.jeefnds.interfaces.ce3.standards;

import java.time.LocalDate;

public interface CommandResponse {

  public void setSum(int num1, int num2);

  public void setFactorialOf(int num);

  public void setCurrentDate();

  public LocalDate getCurrentDate();

  public int getResult();
}
