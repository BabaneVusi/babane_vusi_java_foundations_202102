package main.java.com.psybergate.grad2021.jeefnds.interfaces.ce1v1.vendorimpl2;

import main.java.com.psybergate.grad2021.jeefnds.interfaces.ce1v1.standard.Date;
import main.java.com.psybergate.grad2021.jeefnds.interfaces.ce1v1.standard.InvalidDataException;

public class VendorTwoDate implements Date {

  private int day;

  private int month;

  private int year;

  public VendorTwoDate(int year, int month, int day ) throws InvalidDataException {
    if (day <= 0 || day > 31) {
      throw new InvalidDataException("Invalid day");
    }
    if (month <= 0 || month > 12) {
      throw new InvalidDataException("Invalid month");
    }
    if (year < 1900 || year > 2500) {
      throw new InvalidDataException("Invalid year");
    }

    if (month == 2 && day > 28) {
      throw new InvalidDataException("Invalid day");
    }

    if (month == 4 || month == 6 || month == 9 || month == 11) {
      if (day > 30) {
        throw new InvalidDataException("Invalid day");
      }
    }
    this.day = day;
    this.month = month;
    this.year = year;
  }

  @Override
  public int getDay() {
    return day;
  }

  @Override
  public int getMonth() {
    return month;
  }

  @Override
  public int getYear() {
    return year;
  }

  @Override
  public Date getDate() {
    return null;
  }

  @Override
  public boolean isLeapYear() {
    return false;
  }

  @Override
  public Date addDays(int days) {
    return null;
  }
}
