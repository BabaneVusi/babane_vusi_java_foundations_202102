package main.java.com.psybergate.grad2021.jeefnds.interfaces.ce3.client;

import main.java.com.psybergate.grad2021.jeefnds.interfaces.ce3.vendorimpl1.MyCommandEngine;
import main.java.com.psybergate.grad2021.jeefnds.interfaces.ce3.vendorimpl1.MyCommandRequest;
import com.sun.media.sound.InvalidDataException;

public class CommandClient {

  public static void main(String[] args) throws InvalidDataException {
    MyCommandEngine commandEngine = new MyCommandEngine();
//    MyCommandRequest commandRequest = new MyCommandRequest("sum", 4, 6);
//    System.out.println(commandEngine.processCommand(commandRequest).getResult());

//    MyCommandRequest commandRequest = new MyCommandRequest("factorial", 5);
//    System.out.println(commandEngine.processCommand(commandRequest).getResult());

    MyCommandRequest commandRequest = new MyCommandRequest("current_date");
    System.out.println(commandEngine.processCommand(commandRequest).getCurrentDate());
  }
}
