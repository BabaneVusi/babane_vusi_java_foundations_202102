package main.java.com.psybergate.grad2021.jeefnds.interfaces.ce3.standards;

public interface Command {

  public CommandResponse execute(CommandRequest commandRequest);
}
