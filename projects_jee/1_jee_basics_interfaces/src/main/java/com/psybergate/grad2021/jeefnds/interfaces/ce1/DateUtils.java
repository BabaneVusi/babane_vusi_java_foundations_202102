package main.java.com.psybergate.grad2021.jeefnds.interfaces.ce1;

import com.sun.media.sound.InvalidDataException;

public class DateUtils {

  public static void main(String[] args) throws InvalidDataException {
    MyDateFactory dateFactory = new MyDateFactory();
    System.out.println("dateFactory.getDay() = " + dateFactory.getDay());
    System.out.println("dateFactory.addDays(14) = " + dateFactory.addDays(3));
  }
}
