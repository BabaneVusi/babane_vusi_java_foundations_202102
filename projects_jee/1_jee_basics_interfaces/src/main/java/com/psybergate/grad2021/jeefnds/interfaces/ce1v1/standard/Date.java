package main.java.com.psybergate.grad2021.jeefnds.interfaces.ce1v1.standard;

/**
 * Provides a standard for implementing a date. All implementations of date must be immutable.
 * A DateFactory must be used to create dates.
 * An InvalidDateException must be propagated for trying to create invalid dates.
 */
public interface Date {

  public int getDay();

  public int getMonth();

  public int getYear();

  public Date getDate();

  public boolean isLeapYear();

  /**
   * Adds the number of days to the current date and returns the new date.
   * @return new Date
   */
  public Date addDays(int days) throws InvalidDataException;
}
