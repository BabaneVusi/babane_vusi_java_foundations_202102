package main.java.com.psybergate.grad2021.jeefnds.interfaces.ce3.vendorimpl1;

import main.java.com.psybergate.grad2021.jeefnds.interfaces.ce3.standards.CommandResponse;

import java.time.LocalDate;

public class MyCommandResponse implements CommandResponse {

  private int result;

  private LocalDate currentDate;

  @Override
  public void setSum(int num1, int num2) {
    result = num1 + num2;
  }

  @Override
  public void setFactorialOf(int num) {
    result = factorialOf(num);
  }

  public int factorialOf(int num) {
    if (num == 0) {
      return 1;
    }
    return num * factorialOf(num - 1);
  }

  @Override
  public void setCurrentDate() {
    this.currentDate = LocalDate.now();
  }

  @Override
  public LocalDate getCurrentDate() {
    return currentDate;
  }

  @Override
  public int getResult() {
    return result;
  }
}
