package main.java.com.psybergate.grad2021.jeefnds.interfaces.ce3.vendorimpl1;

import main.java.com.psybergate.grad2021.jeefnds.interfaces.ce3.standards.CommandEngine;
import main.java.com.psybergate.grad2021.jeefnds.interfaces.ce3.standards.CommandRequest;
import main.java.com.psybergate.grad2021.jeefnds.interfaces.ce3.standards.CommandResponse;

public class MyCommandEngine implements CommandEngine {
  @Override
  public CommandResponse processCommand(CommandRequest commandRequest) {
    MyCommand command = new MyCommand();
    return command.execute(commandRequest);
  }
}
