package main.java.com.psybergate.grad2021.jeefnds.interfaces.mydatabase.jdbcdriver;

import main.java.com.psybergate.grad2021.jeefnds.interfaces.mentoring.jdbcdriver.vendorimpl.MyConnection;

import java.sql.*;
import java.util.Properties;
import java.util.logging.Logger;

public class MyDriver implements Driver {

  static {
    try {
      DriverManager.registerDriver(new MyDriver());
    } catch (SQLException throwables) {
      throw new RuntimeException(throwables);
    }
  }

  @Override
  public Connection connect(String url, Properties info) throws SQLException {
    return new MyConnection();
  }

//  public MyConnection connect(String url, Properties info) throws SQLException {
//    return new MyConnection();
//  }

  @Override
  public boolean acceptsURL(String url) throws SQLException {
    return false;
  }

  @Override
  public DriverPropertyInfo[] getPropertyInfo(String url, Properties info) throws SQLException {
    return new DriverPropertyInfo[0];
  }

  @Override
  public int getMajorVersion() {
    return 0;
  }

  @Override
  public int getMinorVersion() {
    return 0;
  }

  @Override
  public boolean jdbcCompliant() {
    return false;
  }

  @Override
  public Logger getParentLogger() throws SQLFeatureNotSupportedException {
    return null;
  }
}
