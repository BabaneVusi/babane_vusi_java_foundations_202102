package main.java.com.psybergate.grad2021.jeefnds.interfaces.ce1v1.client;

import main.java.com.psybergate.grad2021.jeefnds.interfaces.ce1v1.standard.Date;
import main.java.com.psybergate.grad2021.jeefnds.interfaces.ce1v1.standard.DateFactory;
import main.java.com.psybergate.grad2021.jeefnds.interfaces.ce1v1.standard.InvalidDataException;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class DateClient {

  public static final String DATA_PROPERTIES_PATH = "main/resources/date.properties";

  public static void main(String[] args) throws IOException, ClassNotFoundException, IllegalAccessException,
      InstantiationException, InvalidDataException {

    ClassLoader classLoader = DateClient.class.getClassLoader();
    Properties properties = new Properties();

    InputStream inputStream = classLoader.getResourceAsStream(DATA_PROPERTIES_PATH);
    properties.load(inputStream);

    String vendorFactory = properties.getProperty("date.factory");

    Class classObject = Class.forName(vendorFactory);
    DateFactory dateFactory = (DateFactory) classObject.newInstance();

    Date date = dateFactory.createDate(1999, 8, 18);
    System.out.println("date.getMonth() = " + date.getMonth());

//    Date date1 = dateFactory.createDate(17);
//    System.out.println("date1.getDay() = " + date1.getDay());
  }
}
