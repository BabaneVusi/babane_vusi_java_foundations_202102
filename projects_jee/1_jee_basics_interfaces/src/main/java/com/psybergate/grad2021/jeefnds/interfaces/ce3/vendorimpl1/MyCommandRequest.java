package main.java.com.psybergate.grad2021.jeefnds.interfaces.ce3.vendorimpl1;

import main.java.com.psybergate.grad2021.jeefnds.interfaces.ce3.standards.CommandRequest;
import com.sun.media.sound.InvalidDataException;

public class MyCommandRequest implements CommandRequest {

  private String command;

  private int num1;

  private int num2;

  public MyCommandRequest() {
  }

  public MyCommandRequest(String command, int num1, int num2) throws InvalidDataException {
    if (!command.equals("sum") && !command.equals("factorial")) {
      throw new InvalidDataException("Invalid command");
    }
    this.command = command;
    this.num1 = num1;
    this.num2 = num2;
  }

  public MyCommandRequest(String command, int num1) throws InvalidDataException {
    if (!command.equals("factorial")) {
      throw new InvalidDataException("Invalid command");
    }
    this.command = command;
    this.num1 = num1;
  }

  public MyCommandRequest(String command) throws InvalidDataException {
    if (!command.equals("current_date")) {
      throw new InvalidDataException("Invalid command");
    }
    this.command = command;
  }

  @Override
  public void setCommand(String command) throws InvalidDataException {
    if (!command.equals("sum") && !command.equals("factorial") && !command.equals("current_date")) {
      throw new InvalidDataException("Invalid command");
    }
    this.command = command;
  }

  @Override
  public String getCommand() {
    return command;
  }

  public int getNum1() {
    return num1;
  }

  public int getNum2() {
    return num2;
  }
}
