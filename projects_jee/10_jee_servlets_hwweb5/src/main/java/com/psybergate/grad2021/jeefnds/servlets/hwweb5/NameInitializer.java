package com.psybergate.grad2021.jeefnds.servlets.hwweb5;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;
import java.util.ArrayList;
import java.util.List;

@WebListener
public class NameInitializer implements ServletContextListener {

  @Override
  public void contextInitialized(ServletContextEvent sce) {
    List<String> names = new ArrayList<>();

    names.add("Vusi");
    names.add("Clinton");
    names.add("Junior");
    names.add("Buhle");
    names.add("John");
    names.add("Tshepo");
    names.add("Samson");
    names.add("Chief");
    names.add("Tshego");
    names.add("Jones");
    names.add("Yane");
    names.add("Judas");
    names.add("Obim");

    sce.getServletContext().setAttribute("names", names);
  }
}
