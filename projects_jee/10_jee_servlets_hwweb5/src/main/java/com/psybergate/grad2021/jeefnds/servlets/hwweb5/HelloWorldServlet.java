package com.psybergate.grad2021.jeefnds.servlets.hwweb5;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.Random;

@WebServlet(urlPatterns = "/greet/*")
public class HelloWorldServlet extends HttpServlet {

  @Override
  protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException,
          IOException {
    ServletContext sc = getServletContext();
    List<String> names = (List<String>) sc.getAttribute("names");
    System.out.println("Entering doGet version 1.0 with " + names.toString());
    String name = names.get(randomNumber());
    PrintWriter out = resp.getWriter();
    resp.setContentType("text/html");
    out.println("<html><body>");
    out.println("<h2>Hello " + name + "</h2>");
    out.println("</body></html>");
  }

  private int randomNumber() {
    Random random = new Random();
    int upperBound = 13;
    return random.nextInt(upperBound);
  }
}