package com.psybergate.jeefnds.servlets.arch3.ce5c;

public class GreetingsController {

  public String getHelloWorld() {
    return ("Hello World");
  }

  public String greet(String name){
    return "Hello " + name;
  }
}
