package com.psybergate.jeefnds.servlets.arch3.ce5c;

import java.time.LocalDate;

public class DateController {

  public String currentDate() {
    return ("LocalDate.now() = " + LocalDate.now());
  }

  public String tomorrowDate(){
    int year = LocalDate.now().getYear();
    int month = LocalDate.now().getMonthValue();
    int today = LocalDate.now().getDayOfMonth();
    return (LocalDate.of(year, month, today + 1) + "");
  }

}
