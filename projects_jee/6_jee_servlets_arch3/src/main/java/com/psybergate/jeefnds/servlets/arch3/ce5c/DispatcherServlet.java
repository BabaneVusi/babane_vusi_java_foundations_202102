package com.psybergate.jeefnds.servlets.arch3.ce5c;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Map;
import java.util.Properties;

/**
 * DispatcherServlet delegates to controllers that handles multiple request of the same domain.
 * Controllers are loaded dynamically via reflection from a properties file.
 * In the properties, the key value must be similar to the method how in lower cases.
 * In the invoke method, the method name must be include inorder to invoke the specific method.
 */

@WebServlet(value = "/dispatcher/*")
public class DispatcherServlet extends HttpServlet {

  @Override
  protected void doGet(HttpServletRequest req, HttpServletResponse resp)
      throws ServletException, IOException {
    resp.setContentType("text/html");
    PrintWriter out = resp.getWriter();
    out.println("<html><body>");

    String requestType = req.getPathInfo();
    String name = req.getParameter("name");

    InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream("controllers.properties");
    Properties properties = new Properties();
    properties.load(inputStream);

    for (Map.Entry<Object, Object> entry : properties.entrySet()) {
      if (entry.getKey().equals(requestType)) {
        try {
          Class classObject = Class.forName((String) entry.getValue());
          Object instance = classObject.newInstance();
          invokeMethod(out, requestType, classObject, instance, name);
        } catch (Exception e) {
          throw new RuntimeException(e);
        }
      } else if (!properties.containsKey(entry.getKey())) {
        out.println("invalid request");
      }
    }
    out.println("</body></html>");
  }

  private void invokeMethod(PrintWriter out, String requestType, Class classObject, Object instance, String name)
      throws NoSuchMethodException, IllegalAccessException, InvocationTargetException {

    if (requestType.equals("/greet")) {
      Method method = classObject.getMethod("greet", String.class);
      out.println(method.invoke(instance, name));
    } else if (requestType.equals("/helloworld")) {
      Method method = classObject.getMethod("getHelloWorld");
      out.println(method.invoke(instance));
    } else if (requestType.equals("/currentdate")) {
      Method method = classObject.getMethod("currentDate");
      out.println(method.invoke(instance));
    } else if (requestType.equals("/tomorrowdate")) {
      Method method = classObject.getMethod("tomorrowDate");
      out.println(method.invoke(instance));
    } else {
      out.println("Invalid request");
    }
  }
}
